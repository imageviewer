/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <assert.h>
#include <math.h>

#include "rawIOppm.h"

/* convert one line of 422 chroma to 444 */
void RawWriterppm::chroma422to444(const Array2d<int>& in, const int line,
                                  Array1d<int>& out) const
{
	Array1d<int> ChromaLine((in.width()*2) + 2);
	ChromaLine.assign(0);

	/* generate zero stuffed chroma */
	for (int x=0; x<in.width()*2; x+=2) {
		ChromaLine[x+1] = in[line][x/2]; //offset ChromaLine by 1 to get pixel at x=-1
	}

	/* filter */
	for (int x=0; x<in.width()*2; x++) {

		/* 1/2, 1/2 filter *///note the odd indexing to the array
		int C = ChromaLine[x]; //tap at 'x-1'
		C += ChromaLine[x+1] * 2; //tap at 'x'
		C += ChromaLine[x+2]; //tap at 'x+1'

		/* round and shift */
		C++;
		C >>= 1;

		out[x] = C;
	}
}

/* convert a line of YUV to RGB */
void RawWriterppm::yuv2rgb(const Array2d<int>& Y, const int lineNum,
                           const Array1d<int>& ULine, const Array1d<int>& VLine,
                           Array1d<int>& RLine, Array1d<int>& GLine,
                           Array1d<int>& BLine, int depth) const
{
	int maxVal = (1 << depth) - 1;
	//int yOffset = 1 << (depth - 4);

	for (int x=0; x<ULine.length(); x++) {

		//int L = Y[lineNum][x] - yOffset;
		int L = Y[lineNum][x];
		int U = ULine[x];
		int V = VLine[x];

		//Matrix YUV to RGB - Tims, scales output 0-255
		//int R = ((298*L         + 409*V + 128)>>8);
		//int G = ((298*L - 100*U - 208*V + 128)>>8);
		//int B = ((298*L + 516*U         + 128)>>8);

		// Jons - leaves output at same range as input
		int R = ((256*L + 394*V + 128)>>8);
		int G = ((256*L - 46*U - 117*V + 128)>>8);
		int B = ((256*L + 464*U + 128)>>8);

		//printf("Y=%d U=%d V=%d R=%d G=%d B=%d\n", L, U, V, R, G, B);

		//Clip
		RLine[x] = (R < 0) ? 0 : ((R > maxVal) ? maxVal : R);
		GLine[x] = (G < 0) ? 0 : ((G > maxVal) ? maxVal : G);
		BLine[x] = (B < 0) ? 0 : ((B > maxVal) ? maxVal : B);

		//printf("Clipped Y=%d U=%d V=%d R=%d G=%d B=%d\n", L, U, V, RLine[x], GLine[x], BLine[x]);
	}
}

void RawWriterppm::writeCr422(const RawFrame& f) const
{
	int maxVal = (1 << depth) - 1;

	fprintf(outfile, "P6 %d %d %d\n", f.luma.width(), f.luma.height(), maxVal);

	int mask = (1 << (depth - 8)) - 1;

	for (int y=0; y < f.luma.height(); y++) {

		Array1d<int> ULine(f.luma.width());
		Array1d<int> VLine(f.luma.width());

		Array1d<int> RLine(f.luma.width());
		Array1d<int> GLine(f.luma.width());
		Array1d<int> BLine(f.luma.width());

		/* upconvert a line of chroma */
		chroma422to444(f.cb, y, ULine);
		chroma422to444(f.cr, y, VLine);

		/* convert to RGB */
		yuv2rgb(f.luma, y, ULine, VLine, RLine, GLine, BLine, depth);

		for (int x=0; x<f.luma.width(); x++) {

			if (depth > 8) {

				//R
				fputc((RLine[x] >> 8) & mask, outfile);
				fputc((RLine[x] & 0xff), outfile);
				//G
				fputc((GLine[x] >> 8) & mask, outfile);
				fputc((GLine[x] & 0xff), outfile);
				//B
				fputc((BLine[x] >> 8) & mask, outfile);
				fputc((BLine[x] & 0xff), outfile);
			}
			else {
				fputc((RLine[x] & 0xff), outfile);
				fputc((GLine[x] & 0xff), outfile);
				fputc((BLine[x] & 0xff), outfile);
			}
		}
	}
}

void RawWriterppm::writeYOnly(const RawFrame& f) const
{
	/* YYYY (Planar)
	 */
	assert(f.chroma == RawFrame::YOnly);

	fprintf(outfile, "P6 %d %d %d\n", f.luma.width(), f.luma.height(), (1
	    << depth) - 1);

	if (depth > 8) {
		int mask = (1 << (depth - 8)) - 1;
		for (int y = 0; y < f.luma.height(); y++)
			for (int x = 0; x < f.luma.width(); x++) {

				//R
				fputc((f.luma[y][x] >> 8) & mask, outfile);
				fputc((f.luma[y][x] & 0xff), outfile);
				//G
				fputc((f.luma[y][x] >> 8) & mask, outfile);
				fputc((f.luma[y][x] & 0xff), outfile);
				//B
				fputc((f.luma[y][x] >> 8) & mask, outfile);
				fputc((f.luma[y][x] & 0xff), outfile);
			}
	}
	else {
		for (int y = 0; y < f.luma.height(); y++)
			for (int x = 0; x < f.luma.width(); x++) {

				//R
				fputc((f.luma[y][x] & 0xff), outfile);
				//G
				fputc((f.luma[y][x] & 0xff), outfile);
				//B
				fputc((f.luma[y][x] & 0xff), outfile);
			}
	}
}

void RawReaderppm::readHeader()
{
	// This works for our files, but is not generalised:
	fscanf(infile, "P6 %d %d %d\n", &_width, &_height, &_maxval);

	_depth = (int)ceil(log((double)_maxval) / log(2.0));

}

RawFrame& RawReaderppm::read(RawFrame &f)
{
	assert(f.chroma == RawFrame::CrRGB);

	size_t linesRead;
	size_t lineLen;

	if (_maxval > 255) {
		lineLen = f.luma.width() * 3 * 2;
		unsigned char *inLine = new unsigned char[lineLen];

		for (int y = 0; y < _height; y++) {

			linesRead = fread(inLine, lineLen, 1, infile);

			for (int in=0, x=0; x < _width; x++, in+=6) {
				f.r[y][x]=inLine[in+0]*256 + inLine[in+1];
				f.g[y][x]=inLine[in+2]*256 + inLine[in+3];
				f.b[y][x]=inLine[in+4]*256 + inLine[in+5];
			}

			if (linesRead < 1)
				goto exit1;
		}
		exit1: free(inLine);
	}
	else {
		lineLen = f.luma.width() * 3;
		unsigned char *inLine = new unsigned char[lineLen];

		for (int y = 0; y < _height; y++) {

			linesRead = fread(inLine, lineLen, 1, infile);

			for (int in=0, x=0; x < _width; x++, in+=3) {
				f.r[y][x]=inLine[in];
				f.g[y][x]=inLine[in+1];
				f.b[y][x]=inLine[in+2];
			}

			if (linesRead < 1)
				goto exit2;
		}
		exit2: free(inLine);
	}

	return f;
}

RawFrame& RawReaderppm::read()
{
	throw;
}
