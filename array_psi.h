/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 ** The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __ARRAY_H
#define __ARRAY_H

#include <psi.h>

namespace psi
{
/* introduce a new type of dynamic array, that:
 *   - performs bound checking on operator[] -- enabled through (1)
 *   - disables array convolution to reduce mishtakes -- (2)
 */
template<class Target> struct P : MergePolicies
	<ArrayDynamicBase
	,ArrayFirstLastSize
	,ArrayAssign
	/* ,indexZeroBasedNoCheck */
	,IndexingChecked /* (1) */
	,CheckedArrayAccess
	,ArrayArithmetic
	/* ,ArrayConvolution *//* (2) */
	,ArrayIOText
	>::Policy<Target> 
	{
	};
}
;

template<class T> class Array1d : public psi::DynamicArray<T, 1, psi::P> {
public:
	const int length() const
	{
		return Q::size(1);
	}
	;

	Array1d(const int length)
	{
		typename Q::Params init_params;
		/* setup the inital parameters - ie size. */
		init_params.params(1, psi::Size(length));

		/* tell psi about the actual array size we desire. */
		this->params(init_params);
	}
	;

private:
	/* TODO? shaddow copy the width,height -- allows for large performance
	 * increase. */
	typedef psi::DynamicArray<T, 1, psi::P> Q;
};

struct Size2d {
	int width;
	int height;
	Size2d(int w, int h) :
		width(w), height(h)
	{
	}
	;
};

template<class T> class Array2d : public psi::DynamicArray<T, 2, psi::P> {
public:
	const int height() const
	{
		return Q::size(2);
	}
	;
	const int width() const
	{
		return Q::size(1);
	}
	;

	Array2d(const int width, const int height)
	{
		typename Q::Params init_params;
		/* setup the inital parameters - ie size. */
		init_params.params(1, psi::Size(width));
		init_params.params(2, psi::Size(height));

		/* tell psi about the actual array size we desire. */
		this->params(init_params);
	}
	;

	Array2d(const Size2d& dim)
	{
		typename Q::Params init_params;
		/* setup the inital parameters - ie size. */
		init_params.params(1, psi::Size(dim.width));
		init_params.params(2, psi::Size(dim.height));

		/* tell psi about the actual array size we desire. */
		this->params(init_params);
	}
	;

private:
	//    Array2d<T>& operator=(const Array2d<T>&);
	//    Array2d(const Array2d<T>&);
	/* TODO? shaddow copy the width,height -- allows for large performance
	 * increase. */
	typedef psi::DynamicArray<T, 2, psi::P> Q;
};

/* Some magic arrays to get around lack of good references in C++ */
template<class T> class RArray1d {
public:
	RArray1d(const int length) :
		data(length)
	{
	}
	;

	/* make read access look like normal container access */
	T& operator[](const int i) const
	{
		return *(data[i]);
	}
	;

	/* make write access a bit irritating */
	void set(const int i, T& thing)
	{
		data[i] = &thing;
	}
	;
	void set(const int i, T* thing)
	{
		data[i] = thing;
	}
	;

	int length() const
	{
		return data.length();
	}
	;

private:
	/* don't allow copying of this array! */
	RArray1d(const RArray1d<T>&);
	RArray1d<T>& operator=(const RArray1d<T>&);

	Array1d<T*> data;
};

#endif
