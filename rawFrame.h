/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __RAWFRAME_H
#define __RAWFRAME_H

#include <assert.h>

//#include "array_psi.h"
#include "array_df.h"

/* encapsulates the structure of a frame, frames consist of three
 * components, luma, chroma (cr, cb).  Dimensions of the frame may
 * be aquired from each component directly.  alternatively some
 * useful class functions will determine the chroma dimensions
 * given a chroma format convention.
 */
struct RawFrame {
public:
	enum Chroma {CrRGB, Cr444, Cr422, Cr411, Cr420, YOnly};

	/* determine the width of a chroma component, given a chroma format
	 * type and the luma width */
	static int widthOfChroma(Chroma c, int luma_width)
	{
		switch (c) {
		case CrRGB: /* falls through */
		case Cr444:
			return luma_width;
		case Cr422: /* falls through */
		case Cr420:
			return luma_width/2;
		case Cr411:
			return luma_width/4;
		case YOnly:
			return 0;
		}
		throw;
	}
	;

	/* determine the height of a chroma component, given a chroma format
	 * type and the luma height */
	static int heightOfChroma(Chroma c, int luma_height)
	{
		switch (c) {
		case CrRGB: /* falls through */
		case Cr444:
			return luma_height;
		case Cr422:
			return luma_height;
		case Cr411:
			return luma_height;
		case Cr420:
			return luma_height/2;
		case YOnly:
			return 0;
		}
		throw;
	}
	;

	/* determine the chroma type for a frame given the luma and chroma
	 * components */
	static Chroma chromaFromRatio(const Array2d<int>& compY,
	                              const Array2d<int>& compCr,
	                              const Array2d<int>& compCb)
	{
		/* If the two components are not of equal size, it is not a valid
		 * frame */
		assert(compCr.height() == compCb.height());
		assert(compCr.width() == compCb.width());

		/* attempt to find the chroma type */
		if (compCr.height() == 0 && compCr.width() == 0)
			return YOnly;

		/* Look for H subsampling */
		if (compY.width() == compCr.width() * 4) {
			return Cr411;
		}
		if (compY.width() == compCr.width() * 2) {
			if (compY.height() == compCr.height())
				return Cr422;
			if (compY.height() == compCr.height() * 2)
				return Cr420;
		}

		return Cr444;
		throw /* some exception saying invalid ratio */;
	}
	;

	/* this should check for sensible image dimensions too? */
	RawFrame(int width, int height, enum Chroma chroma) :
		chroma(chroma), luma(width, height),
		    cr(widthOfChroma(chroma, width), heightOfChroma(chroma, height)),
		    cb(widthOfChroma(chroma, width), heightOfChroma(chroma, height)),
		    g(luma), r(cr), b(cb)
	{
	}
	;

	/* this should check for sensible image dimensions too? */
	//    RawFrame(const Size2d &size, enum Chroma chroma)
	//        : chroma(chroma)
	//        , luma(size.width, size.height)
	//        , cr(widthOfChroma(chroma, size.width), heightOfChroma(chroma, size.height))
	//        , cb(widthOfChroma(chroma, size.width), heightOfChroma(chroma, size.height))
	//        , g(luma)
	//        , r(cr)
	//        , b(cb)
	//        {};

	/* convert a single component to a YOnly frame */
	RawFrame(const Array2d<int>& component) :
		chroma(YOnly), luma(component.width(), component.height()), cr(0, 0) /* initialize chroma to a zero sized array */
		, cb(0, 0) /* initialize chroma to a zero sized array */
		, g(luma), r(cr), b(cb)
	{
		luma = component;
	}
	;

	/* convert three seperate components to the apropriate frame type */
	/* there is some irritating fun here - To determine the chroma type,
	 * one needs to check the correct ratios of components.  However,
	 * if there is a fault in the ratios, then an exception must be
	 * thrown. Blame C++. */
	RawFrame(const Array2d<int>& compY, const Array2d<int>& compCr,
	         const Array2d<int>& compCb) :
		    chroma(chromaFromRatio(compY, compCr, compCb)) //new arrays with size of input arrays
		    , luma(compY.width(), compY.height()), cr(compCr.width(),
		                                              compCr.height()),
		    cb(compCb.width(), compCb.height()), g(luma) //intialise references
		    , r(cr), b(cb)
	{
		//copy input data to new arrays
		luma = compY;
		cr = compCr;
		cb = compCb;
	}
	;

	const Chroma chroma;
	/* food for thought: these should probably become references,
	 * if allocations are needed, then allocate, otherwise use the
	 * reference. */
	Array2d<int> luma;
	Array2d<int> cr;
	Array2d<int> cb;

	Array2d<int>& g;
	Array2d<int>& r;
	Array2d<int>& b;
};

#endif
