/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <QtGui>

#include "transformDialog.h"

TransformDialog::TransformDialog(const QString &name, QWidget *parent) :
	QDialog(parent)
{
	setWindowTitle(name);

	//zoom
	zoomLabel = new QLabel(tr("Zoom"));

	zoomReset = new QPushButton();
	zoomReset->setText(tr("Reset"));
	connect(zoomReset, SIGNAL(clicked()), this, SLOT(resetZoom()));

	zoomNumber = new QDoubleSpinBox;
	zoomNumber->setMinimum(0.1);
	zoomNumber->setMaximum(32);
	zoomNumber->setSingleStep(0.1);
	connect(zoomNumber, SIGNAL(valueChanged(double)), this, SLOT(zoomNumberChanged(double)));

	zoomSlider = new QSlider;
	zoomSlider->setOrientation(Qt::Horizontal);
	zoomSlider->setMinimum(1);
	zoomSlider->setMaximum(320);
	zoomSlider->setValue(100);
	connect(zoomSlider, SIGNAL(valueChanged(int)), this, SLOT(zoomSliderChanged(int)));

	//rotation
	rotationLabel = new QLabel(tr("Rotation"));

	rotationReset = new QPushButton;
	rotationReset->setText(tr("Reset"));
	connect(rotationReset, SIGNAL(clicked()), this, SLOT(resetRotation()));

	rotationNumber = new QSpinBox;
	rotationNumber->setMinimum(-360);
	rotationNumber->setMaximum(360);
	rotationNumber->setValue(0);
	connect(rotationNumber, SIGNAL(valueChanged(int)), this, SLOT(rotationValueChanged(int)));

	rotationSlider = new QSlider;
	rotationSlider->setOrientation(Qt::Horizontal);
	rotationSlider->setMinimum(-360);
	rotationSlider->setMaximum(360);
	rotationSlider->setValue(0);
	connect(rotationSlider, SIGNAL(valueChanged(int)), this, SLOT(rotationValueChanged(int)));

	//layout
	QGridLayout *layout = new QGridLayout;
	layout->addWidget(zoomLabel, 0, 0);
	layout->addWidget(zoomReset, 0, 1);
	layout->addWidget(zoomNumber, 0, 2);
	layout->addWidget(zoomSlider, 0, 3);

	layout->addWidget(rotationLabel, 1, 0);
	layout->addWidget(rotationReset, 1, 1);
	layout->addWidget(rotationNumber, 1, 2);
	layout->addWidget(rotationSlider, 1, 3);

	setLayout(layout);
}

//----------------------------------------------------------------------------------
// Zoom Control
void TransformDialog::resetZoom(void)
{

	zoomNumber->setValue(1.0);
	zoomSlider->setValue(10);

	if (zoomRatio != 1.0) {
		zoomRatio = 1.0;
		emit(zoomChanged(zoomRatio));
	}
}

//a number is entered into the zoom box
void TransformDialog::zoomNumberChanged(double value)
{

	zoomSlider->setValue((int)(value * 10));

	if (zoomRatio != value) {
		emit(zoomChanged(value));
		zoomRatio = value;
	}
}

//the zoom slider is moved
void TransformDialog::zoomSliderChanged(int value)
{

	float ratio = (float)value / 10.0;
	zoomNumber->setValue(ratio);

	if (zoomRatio != ratio) {
		emit(zoomChanged(ratio));
		zoomRatio = ratio;
	}
}

//----------------------------------------------------------------------------------
// Rotation Control
void TransformDialog::resetRotation(void)
{

	rotationNumber->setValue(0);
	rotationSlider->setValue(0);

	if (rotation != 0) {
		rotation = 0;
		emit(rotationChanged(rotation));
	}
}

//a number is entered into the zoom box, or the slider moved
//these can be handles by the same function as they are both integers
void TransformDialog::rotationValueChanged(int value)
{

	rotationSlider->setValue(value);
	rotationNumber->setValue(value);

	if (rotation != value) {
		emit(rotationChanged(value));
		rotation = value;
	}
}

//----------------------------------------------------------------------------------
// Initialisation
void TransformDialog::setZoom(float value)
{

	zoomRatio = value;
	zoomNumber->setValue(zoomRatio);
	zoomSlider->setValue((int)(zoomRatio * 10));
}

void TransformDialog::setRotation(int value)
{

	rotation = value;
	rotationSlider->setValue(value);
	rotationNumber->setValue(value);
}
