<!DOCTYPE DCF>

<assistantconfig version="3.2.0">

<profile>
    <property name="name">imageviewer</property>
    <property name="title">Imageviewer Help</property>
    <property name="applicationicon">images/handbook.png</property>
    <property name="startpage">index.html</property>
    <property name="aboutmenutext">About Imageviewer</property>
    <property name="abouturl">about.txt</property>
    <property name="assistantdocs">.</property>
</profile>

<DCF ref="./index.html" icon="./images/handbook.png" title="Imageviewer">

	<section ref="openfile.html" title="Open File" /> 
	<section ref="videoformats.html" title="Video Formats" /> 
	<section ref="imagesize.html" title="Image Size" />
	<section ref="bitdepth.html" title="Bit Depth" />
	<section ref="yuvdisplay.html" title="Display Format" />         
	<section ref="zoom.html" title="Image Zoom" />         
	<section ref="numberrange.html" title="Video Number Range" />
	<section ref="colour.html" title="Colour Space Conversion" />
	<section ref="fullscreen.html" title="Full Screen Mode" />
	<section ref="bugs.html" title="Bugs/Limitations" />         

</DCF>

</assistantconfig>