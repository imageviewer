/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

//SGI file format header
//      2 bytes | short  | MAGIC     | IRIS image file magic number
//      1 byte  | char   | STORAGE   | Storage format,    0=VERBATIM, 1=RLE
//      1 byte  | char   | BPC       | Number of bytes per pixel channel
//      2 bytes | ushort | DIMENSION | Number of dimensions, 1=1 line of XSIZE; 2=1 plane of XSIZE*YSIZE (greyscale image); 3=ZSIZE planes of XSIZE*YSIZE (RGB image)
//      2 bytes | ushort | XSIZE     | X size in pixels
//      2 bytes | ushort | YSIZE     | Y size in pixels
//      2 bytes | ushort | ZSIZE     | Number of channels
//      4 bytes | long   | PIXMIN    | Minimum pixel value
//      4 bytes | long   | PIXMAX    | Maximum pixel value
//      4 bytes | char   | DUMMY     | Ignored
//     80 bytes | char   | IMAGENAME | Image name
//      4 bytes | long   | COLORMAP  | Colormap ID
//    404 bytes | char   | DUMMY     | Ignored


#define DEBUG 0

#include <assert.h>

#include "rawIOsgi.h"

//read a 16 bit value, endian safe
inline unsigned short getshort(FILE *inf)
{
	unsigned char buf[2];

	fread(buf, 2, 1, inf);
	return (buf[0]<<8)+(buf[1]<<0);
}

//read a 32 bit value, endian safe
inline long getlong(FILE *inf)
{
	unsigned char buf[4];

	fread(buf, 4, 1, inf);
	return (buf[0]<<24)+(buf[1]<<16)+(buf[2]<<8)+(buf[3]<<0);
}

void RawReadersgi::readHeader()
{
	_magic = getshort(infile);

	//bail out if this does not look like an SGI file
	if (_magic != 474)
		return;

	fread(&_storage, 1, 1, infile);
	fread(&_bpc, 1, 1, infile);
	_dimension = getshort(infile);
	_width = getshort(infile);
	_height = getshort(infile);
	_zsize = getshort(infile);
	_pixmin = getlong(infile);
	_pixmax = getlong(infile);
	fseek(infile, 4, SEEK_CUR); //skip dummy 4 bytes
	fread(&_name, 80, 1, infile); //80 bytes name
	_name[80]='\0'; //force string termination
	_colormap = getlong(infile);
	fseek(infile, 404, SEEK_CUR); //skip dummy 404 bytes

	//set depth to 8 or 16 bits
	_depth=8;
	if (_bpc==2)
		_depth=16;

	//we are now 512 bytes into the file, which is the end of the header
	if (DEBUG) {
		printf("MAGIC      = %d\n", _magic);
		printf("STORAGE    = %d\n", _storage);
		printf("BPC        = %d\n", _bpc);
		printf("DIMENSIONS = %d\n", _dimension);
		printf("XSIZE      = %d\n", _width);
		printf("YSIZE      = %d\n", _height);
		printf("ZSIZE      = %d\n", _zsize);
		printf("PIXMIN     = %ld\n", _pixmin);
		printf("PIXMAX     = %ld\n", _pixmax);
		printf("NAME       = %s\n", _name);
		printf("COLORMAP   = %ld\n", _colormap);
	}
}

void RawReadersgi::read_plane(Array2d<int> &p, int ysize)
{
	size_t linesRead;
	//note that the scan lines are stored in reverse order, bottom to top of frame


	switch (_bpc) {
	case 1:
	{
		//8 bits per channel
		unsigned char *inLine = new unsigned char[p.width()];

		for (int y=ysize-1; y>=0; y--) {
			linesRead = fread(inLine, p.width(), 1, infile);

			for (int x = 0; x < p.width(); x++)
				p[y][x] = inLine[x];

			if (linesRead < 1)
				goto exit;
		}
	}
		break;

	case 2:
	{
		//16 bits per channel
		unsigned char *inLine = new unsigned char[p.width()*2];
		unsigned char *ptr;
		for (int y=ysize-1; y>=0; y--) {

			linesRead = fread(inLine, p.width()*2, 1, infile);
			ptr = inLine;
			for (int x = 0; x < p.width(); x++, ptr+=2)
				p[y][x] = (ptr[0] << 8) | ptr[1];

			if (linesRead < 1)
				goto exit;
		}
	}
		break;

	default:
		break;
		//undefined bpc
	}

	exit: ;

}

RawFrame& RawReadersgi::read(RawFrame &f)
{
	assert(f.chroma == RawFrame::CrRGB);

	//we don't deal with run length coded files yet
	if (_storage != 0)
		return f;

	switch (_dimension) {
	case 1:
		//there is one row of XSIZE pixels
		read_plane(f.r, 1);
		break;

	case 2:
		//there is one plane of XSIZE * YSIZE
		read_plane(f.r, _height);
		break;

	case 3:
		//there are ZSIZE planes of XSIZE * YSIZE
		if (_zsize == 1) {
			//greyscale
			read_plane(f.r, _height);
			f.g = f.r; //copy red plane to blue and green to get greyscale. Inefficient, but works
			f.b = f.r;
		}

		if (_zsize == 3 || _zsize == 4) {
			//RGB or RGBA
			read_plane(f.r, _height);
			read_plane(f.g, _height);
			read_plane(f.b, _height);
		}
		break;
	}

	return f;
}

RawFrame& RawReadersgi::read()
{
	throw;
}

