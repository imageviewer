/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include "sequenceReader.h"

#include "rawIOv216.h"
#include "rawIOv210.h"
#include "rawIOYV12.h"
#include "rawIOuyvy.h"
#include "rawIOpgm.h"
#include "rawIOppm.h"
#include "rawIOuy16.h"
#include "rawIOi420.h"
#include "rawIOyy16.h"
#include "rawIOyyy8.h"
#include "rawIO16P4.h"
#include "rawIO16P2.h"
#include "rawIO16P0.h"
#include "rawIOsgi.h"
#include "rawIO8P2.h"
#include "rawIOrgb.h"
#include "rawIOrgba.h"
#include "rawIOleader-frm.h"

#include <iostream>
#include <QtCore>

#include <stdio.h>

#define _FILE_OFFSET_BITS 64

#define DEBUG 0

SequenceReader::SequenceReader() :
	QObject()
{
	filePtr = NULL;
	destFrame = new RawFrame(0, 0, RawFrame::Cr422); //bogus frame for now
	firstFrame = 0;
	lastFrame = 0;
	currentFrameNum = 0;
	currentFileIndex = 0;

	//there must be a better way that this to initialise these arrays?
	rawImageWidth[SizeQSIF] = 176;      rawImageHeight[SizeQSIF] = 120;
	rawImageWidth[SizeQCIF] = 176;      rawImageHeight[SizeQCIF] = 144;
	rawImageWidth[SizeSIF] = 352;       rawImageHeight[SizeSIF] = 240;
	rawImageWidth[SizeCIF] = 352;       rawImageHeight[SizeCIF] = 288;
	rawImageWidth[Size4SIF] = 704;      rawImageHeight[Size4SIF] = 480;
	rawImageWidth[Size4CIF] = 704;      rawImageHeight[Size4CIF] = 576;
	rawImageWidth[SizeSD480] = 720;     rawImageHeight[SizeSD480] = 480;
	rawImageWidth[SizeSD576] = 720;     rawImageHeight[SizeSD576] = 576;
	rawImageWidth[SizeHD720] = 1280;    rawImageHeight[SizeHD720] = 720;
	rawImageWidth[SizeHD1080] = 1920;   rawImageHeight[SizeHD1080] = 1080;
	rawImageWidth[Size2K] = 2048;       rawImageHeight[Size2K] = 1556;
	rawImageWidth[Size4K] = 4096;       rawImageHeight[Size4K] = 3112;
	rawImageWidth[SizeSHV] = 7680;      rawImageHeight[SizeSHV] = 4320;
	rawImageWidth[SizeCustom] = 640;    rawImageHeight[SizeCustom] = 480;
}

int SequenceReader::getSequenceLength(void)
{
	if (fileList.size() == 0)
		return 0;

	return lastFrame - firstFrame + 1;
}

int SequenceReader::getFirstFrameNum(void)
{
	return firstFrame;
}

int SequenceReader::getLastFrameNum(void)
{
	return lastFrame;
}

int SequenceReader::getCurrentFrameNum(void)
{
	return currentFrameNum;
}

//identify those file types which contain a header giving image dimensions and maybe other useful stuff
bool SequenceReader::hasHeader(RawType type)
{
	bool header;

	switch (type) {
	case type_leader_frm:
	case type_pgm:
	case type_ppm:
	case type_sgi:
		header = true;
		break;

	default:
		header = false;
		break;
	}

	return header;
}

//for raw file types we can sometimes know the bit depth given the file extension
int SequenceReader::bitDepth(RawType type)
{
	int depth = 0;

	switch (type) {
	case type_uy16:
		depth = 0; //we don't know what the depth of this file is
		break;

	case type_v210:
		depth = 10;
		break;

	case type_v216:
	case type_yy16:
	case type_16P4:
	case type_16P2:
	case type_16P0:
		depth = 16;
		break;

	case type_i420:
	case type_rgb:
	case type_rgba:
	case type_yv12:
	case type_uyvy:
	case type_yyy8:
	case type_8P2:
		depth = 8;
		break;

	default:
		depth = 0;
		break;
	}

	return depth;
}

//convert a file extension into the enumerated file type
SequenceReader::RawType SequenceReader::rawType(QString &ext)
{
	RawType type;

	ext = ext.toLower();

	if (ext == "v210") {
		type = type_v210;
	}
	else if (ext == "v216") {
		type = type_v216;
	}
	else if (ext == "uy16") {
		type = type_uy16;
	}
	else if (ext == "yv12") {
		type = type_yv12;
	}
	else if (ext == "i420") {
		type = type_i420;
	}
	else if (ext == "uyvy") {
		type = type_uyvy;
	}
	else if (ext == "yy16") {
		type = type_yy16;
	}
	else if (ext == "yyy8") {
		type = type_yyy8;
	}
	else if (ext == "16p4") {
		type = type_16P4;
	}
	else if (ext == "16p2") {
		type = type_16P2;
	}
	else if (ext == "16p0") {
		type = type_16P0;
	}
	else if (ext == "frm" || ext == "FRM") {
		type = type_leader_frm;
	}
	else if (ext == "pgm") {
		type = type_pgm;
	}
	else if (ext == "pnm") { //hack
		type = type_ppm;
	}
	else if (ext == "ppm") {
		type = type_ppm;
	}
	else if (ext == "sgi") {
		type = type_sgi;
	}
	else if (ext == "8p2") {
		type = type_8P2;
	}
	else if (ext == "rgb")
	{
	  type = type_rgb;
	}
	else if (ext == "rgba")
	{
		type = type_rgba;
	}
	else {
		type = type_unknown;
	}

	return type;
}

//convert a file extension into enumerated chroma type
RawFrame::Chroma SequenceReader::chromaType(RawType type)
{
	RawFrame::Chroma inChroma;

	switch (type) {
	case type_v210:
	case type_v216:
	case type_uy16:
	case type_uyvy:
	case type_16P2:
	case type_8P2:
		inChroma = RawFrame::Cr422;
		break;

	case type_i420:
	case type_yv12:
	case type_16P0:
		inChroma = RawFrame::Cr420;
		break;

	case type_16P4:
		inChroma = RawFrame::Cr444;
		break;

	case type_yy16:
	case type_yyy8:
	case type_pgm:
		inChroma = RawFrame::YOnly;
		break;

	case type_ppm:
	case type_sgi:
	case type_rgb:
	case type_rgba:
		inChroma = RawFrame::CrRGB;
		break;

	case type_leader_frm: /* for leader, this is overridden later */
	default:
		inChroma = RawFrame::Cr422;
		break;

	}

	return inChroma;
}
//return the number of bytes per frame for simple concatenated file types, or 0 for other types
int SequenceReader::bytesPerFrame(RawType type)
{
	int bpf=0;

	switch (type) {

	case type_16P4:
		bpf = rawWidth * rawHeight * 2 * 3;
		break;

	case type_v210:
		bpf = ((rawWidth + 47)/48) * 48 * rawHeight * 2 * 4 / 3;
		break;

	case type_v216:
	case type_uy16:
	case type_16P2:
		bpf = rawWidth * rawHeight * 2 * 2;
		break;

	case type_16P0:
		bpf = rawWidth * rawHeight * 2 * 3 / 2;
		break;

	case type_i420:
	case type_yv12:
		bpf = rawWidth * rawHeight * 3 / 2;
		break;

	case type_uyvy:
	case type_8P2:
		bpf = rawWidth * rawHeight * 2;
		break;

	case type_yyy8:
		bpf = rawWidth * rawHeight;
		break;

	case type_yy16:
		bpf = rawWidth * rawHeight * 2;
		break;

	case type_rgb:
    bpf = rawWidth * rawHeight * 3;
    break;

	case type_rgba:
    bpf = rawWidth * rawHeight * 4;
    break;

	default:
		bpf = 0;
		break;
	}

	return bpf;
}

void SequenceReader::setRawFormat(RawType format)
{
	frameType = format;

	//if we have any files, the info will have changed
	if (fileList.size() > 0)
		buildFileInfo();

	readFrame(currentFrameNum);
}

//open a directory full of files
int SequenceReader::setDirectoryOfFiles(const QString &dir)
{
	//get directory contents with the extension of the selected file
	//sorted by name, selecting only files not directories
	QDir
	    dirInfo(
	            dir,
	            QString("*.v210 *.uy16 *.v216 *.yv12 *.i420 *.16P4 *.16P2 *16P0 *.uyvy *.yuv *.yyy8 *.yy16 *.8P2 *.rgb *.rgba"),
	            QDir::Name, QDir::Files);

	if (dirInfo.count() == 0)
		return -1;

	QFileInfoList fileInfoList = dirInfo.entryInfoList();
	QStringList list;
	for (unsigned int i=0; i<dirInfo.count(); i++)
		list.append(fileInfoList[i].absoluteFilePath());

	fileList = list; //replace the old file list with the new one

	if (filePtr != NULL) {
		fclose(filePtr);
		filePtr = NULL;
	}

	//build file information for the new file list
	buildFileInfo();

	//success
	return 0;
}

//given a list of filenames, generate the complete list of filenames in a sequence
//if the filename is numerical, subsequent files of the same extension are added to the list
int SequenceReader::setFileNames(const QStringList &newFileList)
{
	//check for empty list
	if (newFileList.size() == 0) {
		//tried to open an empty file list
		return -1;
	}

	//check that all the files in the list have a recognised extension
	for (int i=0; i<newFileList.size(); i++) {
		QFileInfo info(newFileList.at(i));
		QString ext = info.suffix();

		//look for unknown file extensions
		if ((rawType(ext) == type_unknown) && (frameType == type_auto))
			//unknown file extension
			return -2;
	}

	QStringList list = newFileList;
	QFileInfo firstFileInfo(list.at(0));
	bool firstIsNumerical = false;
	firstFileInfo.baseName().toInt(&firstIsNumerical, 10);

	//if there is a single entry in the file list, and the file name is a number,
	//look for other files _after_ this one with the same extension, sorted by name
	if (list.size() == 1 && firstIsNumerical) {

		QString ext = firstFileInfo.suffix();
		QString name = firstFileInfo.fileName();

		//get directory contents with the extension of the selected file
		//sorted by name, selecting only files not directories
		QDir dirInfo(firstFileInfo.absolutePath(), QString("*." + ext),
		             QDir::Name, QDir::Files);

		//it is possible to iterate over the dirInfo.entryList, but this is horribly slow
		//instead, get a QFileInfoList which is very fast to access
		QFileInfoList fileInfoList = dirInfo.entryInfoList();

		//add to the file list all files which occur after the specified one
		bool found = false;
		for (int i = 0; i < fileInfoList.size(); ++i) {

			//once we have found the selected file in the directory, add subsequent ones to the file list
			if (found)
				list.append(fileInfoList[i].absoluteFilePath());

			//detect when the selected file has been found in the directory listing
			if (found == false && (fileInfoList[i].fileName() == name))
				found = true;
		}
	}

	//we have built the new file list. Use this instead of the old one now.
	fileList = list;
	currentFileIndex = 0;
	if (filePtr != NULL) {
		fclose(filePtr);
		filePtr = NULL;
	}

	//build file information for the new file list
	buildFileInfo();

	//success
	return 0;
}

void SequenceReader::buildFileInfo()
{
	infoList.clear();
	QFileInfo firstFileInfo(fileList.at(0));

	firstFrame = firstFileInfo.baseName().toInt(NULL, 10);
	int firstInFile = firstFrame;
	lastFrame = firstFrame;

	//for each file in the list, get the info on it
	for (int i=0; i<fileList.size(); i++) {

		QFileInfo fileInfo(fileList.at(i));
		QString ext = fileInfo.suffix();

		sequenceFileInfo sfi;

		if (frameType == type_auto)
			sfi.rawType = rawType(ext);
		else
			sfi.rawType = frameType;

		sfi.bytesPerFrame = bytesPerFrame(sfi.rawType);

		//determine the number of frames in a file, avoiding a division by zero
		if (sfi.bytesPerFrame > 0) {
			sfi.framesInFile = (int)(fileInfo.size() / sfi.bytesPerFrame);
		}
		else {
			sfi.framesInFile = 0;
		}

		//there is at least one frame, even if the file is too small for bytesPerFrame
		if (sfi.framesInFile == 0)
			sfi.framesInFile = 1;

		sfi.bitDepth = bitDepth(sfi.rawType);
		sfi.hasHeader = hasHeader(sfi.rawType);
		sfi.chroma = chromaType(sfi.rawType);
		sfi.firstFrameNum = firstInFile;
		sfi.lastFrameNum = firstInFile + sfi.framesInFile - 1;

		infoList.push_back(sfi);

		firstInFile += sfi.framesInFile; //first frame number in next file
		lastFrame = sfi.lastFrameNum; //last frame number in the sequence
	}

	if (DEBUG) {

		printf("First frame is %d\n", firstFrame);
		printf("Last frame  is %d\n", lastFrame);
		printf("Raw width   is %d\n", rawWidth);
		printf("Raw Height  is %d\n", rawHeight);

		//dump the file list, for interest
		for (int i=0; i<fileList.size(); i++) {
			printf("File list entry %d (%s)\n", i, fileList.at(i).toLatin1().data());
			printf("    File info bytesPerFrame = %d\n", infoList.at(i).bytesPerFrame);
			printf("    File info chroma        = %d\n", (int)infoList.at(i).chroma);
			printf("    File info has header    = %d\n", (int)infoList.at(i).hasHeader);
			printf("    File info bit depth     = %d\n", infoList.at(i).bitDepth);
			printf("    File info framesInFile  = %d\n", infoList.at(i).framesInFile);
			printf("    File info firstFrameNum = %d\n", infoList.at(i).firstFrameNum);
			printf("    File info lastFrameNum  = %d\n", infoList.at(i).lastFrameNum);
		}
	}

	//sanity check on currentFrameNum, as the frame numbering may have changed
	if (currentFrameNum < firstFrame)
		currentFrameNum = firstFrame;

	if (currentFrameNum > lastFrame)
		currentFrameNum = lastFrame;
}

int SequenceReader::fileIndexForFrameNum(int frameNum)
{
	int index=0;

	if (frameNum > currentFrameNum) {

		//search forwards
		for (int i=currentFileIndex; i<(int)infoList.size(); i++) {

			if (frameNum >= infoList.at(i).firstFrameNum && frameNum <= infoList.at(i).lastFrameNum) {
				index = i;
				break;
			}
		}

	}
	else {

		//search backwards
		for (int i=currentFileIndex; i>0; i--) {
			if (frameNum >= infoList.at(i).firstFrameNum && frameNum <= infoList.at(i).lastFrameNum) {
				index = i;
				break;
			}
		}

	}

	return index;
}

void SequenceReader::nextInSequence()
{
	readFrame(currentFrameNum+1);
}

void SequenceReader::prevInSequence()
{
	readFrame(currentFrameNum-1);
}

//move to a frame number in the sequence
int SequenceReader::jumpInSequence(int wantedFrame)
{
	//santiy check
	if (wantedFrame > lastFrame || wantedFrame < firstFrame)
		return -1;

	if(fileList.size() < 1)
		return -1;

	//get the file index for the wanted frame number
	int newFileIndex = fileIndexForFrameNum(wantedFrame);

	//see if we have that file open at the moment
	if (newFileIndex != currentFileIndex || filePtr == NULL) {
		//open a the new file
		currentFileIndex = newFileIndex;

		//open the new file
		if (filePtr != NULL)
			fclose(filePtr);
		filePtr = fopen(fileList.at(currentFileIndex).toLatin1().data(), "rb");
		emit(frameFileName(fileList.at(currentFileIndex)));
	}

	if (filePtr == NULL) {
		return -2;
	}

	//seek to the position of the wanted frame in the current file
	off_t frameOffset = wantedFrame - infoList.at(currentFileIndex).firstFrameNum;
	off_t byteOffset = frameOffset * infoList.at(currentFileIndex).bytesPerFrame;

#ifdef Q_OS_WIN32
#ifdef Q_CC_MSVC
#if _MSC_VER >= 1400
	if(_fseeki64(filePtr, byteOffset, SEEK_SET)) {
#else
		if(fseek(filePtr, byteOffset, SEEK_SET)) {
#endif
#endif

#ifdef Q_CC_GNU
			if(fseeko64(filePtr, byteOffset, SEEK_SET)) {
#endif
#endif

#ifdef Q_OS_UNIX
	if(fseeko(filePtr, byteOffset, SEEK_SET)) {
#endif
	return -3;
	}

	//all done
	currentFrameNum = wantedFrame;

	QFileInfo fi(fileList.at(currentFileIndex));

	emit(sequencePosition(fi.fileName(), currentFrameNum - infoList.at(currentFileIndex).firstFrameNum, firstFrame, currentFrameNum, lastFrame));

	//success
	return 0;
}

//read a particular frame number
RawFrame & SequenceReader::readFrame(int frameNum)
{
	if(fileList.size()> 0) {

		jumpInSequence(frameNum);

		RawReader *r=NULL;

		//select either the frame type from the extension, or the user selected override
		RawType type;

		if(infoList.at(currentFileIndex).hasHeader) {
			type = infoList.at(currentFileIndex).rawType;
		}
		else {
			type = (frameType == type_auto) ? infoList.at(currentFileIndex).rawType : frameType;
		}

		switch(type) {

			case type_yyy8:
				emit(frameFormat(QString("YYY8 (Yonly) 8Bit")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderYYY8(filePtr);
				break;

			case type_yy16:
				emit(frameFormat(QString("YY16 (Yonly) 16Bit")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderYY16(filePtr);
				break;

			case type_v216:
				emit(frameFormat(QString("V216 4:2:2 16Bit muxed")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderV216(filePtr);
				break;

			case type_uy16:
				emit(frameFormat(QString("UY16 4:2:2 16Bit muxed")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderUY16(filePtr);
				break;

			case type_v210:
				emit(frameFormat(QString("V210 4:2:2 10Bit packed")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderv210(filePtr);
				break;

			case type_yv12:
				emit(frameFormat(QString("YV12 4:2:0 8Bit planar YVU")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderYV12(filePtr);
				break;

			case type_i420:
				emit(frameFormat(QString("I420 4:2:0 8Bit planar YUV")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderi420(filePtr);
				break;

			case type_16P4:
				emit(frameFormat(QString("16P4 4:4:4 16Bit planar YUV")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReader16P4(filePtr);
				break;

			case type_16P2:
				emit(frameFormat(QString("16P2 4:2:2 16Bit planar YUV")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReader16P2(filePtr);
				break;

			case type_16P0:
				emit(frameFormat(QString("16P0 4:2:0 16Bit planar YUV")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReader16P0(filePtr);
				break;

			case type_uyvy:
				emit(frameFormat(QString("UYVY 4:2:2 8Bit muxed")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderuyvy(filePtr);
				break;

			case type_rgb:
				emit(frameFormat(QString("RGB 8Bit muxed")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderrgb(filePtr);
				break;

			case type_rgba:
				emit(frameFormat(QString("RGBA 8Bit muxed")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				r = new RawReaderrgba(filePtr);
				break;

			case type_leader_frm:
				r = new RawReaderLeaderFRM(filePtr);
				emit(frameFormat(QString("Leader RAW FRM (%1 bits)") .arg(r->getDepth())));
				emit(frameBitDepth(r->getDepth()));
				break;

			case type_pgm:
				r = new RawReaderpgm(filePtr);
				emit(frameFormat(QString("Portable Greymap (%1 bits)") .arg(r->getDepth())));
				emit(frameBitDepth(r->getDepth()));
				break;

			case type_ppm:
				r = new RawReaderppm(filePtr);
				emit(frameFormat(QString("Portable Pixmap (%1 bits)") .arg(r->getDepth())));
				emit(frameBitDepth(r->getDepth()));
				break;

			case type_sgi:
				r = new RawReadersgi(filePtr);
				emit(frameFormat(QString("SGI file (%1 bits)") .arg(r->getDepth())));
				emit(frameBitDepth(r->getDepth()));
				break;

			case type_8P2:
				r = new RawReader8P2(filePtr);
				emit(frameFormat(QString("8P2 4:2:2 8Bit planar YUV")));
				emit(frameBitDepth(infoList.at(currentFileIndex).bitDepth));
				break;

			default:
				emit(frameFormat(QString("File of unknown type '%1'") .arg(fileList.at(currentFileIndex))));
				emit(frameBitDepth(0));
				break;
		}

		//check if the frame dimensions can be read from the frame header
		emit(frameHasHeader(infoList.at(currentFileIndex).hasHeader));

		if(infoList.at(currentFileIndex).hasHeader) {
			frameWidth = r->getWidth();
			frameHeight = r->getHeight();
			frameDepth = r->getDepth();
		}
		else {
			frameDepth = infoList.at(currentFileIndex).bitDepth;
			frameWidth = rawWidth;
			frameHeight = rawHeight;
		}

		//determine the chroma type that we will read into
		//this is set either by the file extension or the image->image format menu
		RawFrame::Chroma chroma;
		if (frameType == type_leader_frm)
			chroma = ((RawReaderLeaderFRM*)r)->getChroma();
		else if(frameType == type_auto)
			chroma = infoList.at(currentFileIndex).chroma;
		else
			chroma = chromaType(frameType);

		//sanity checks - is the destination frame type correct?
		if(destFrame->chroma != chroma ||
			destFrame->luma.width() != frameWidth ||
			destFrame->luma.height() != frameHeight) {
			delete destFrame;
			destFrame = new RawFrame(frameWidth, frameHeight, chroma);
		}

		//read the frame
		if(r != NULL) {
			r->read(*destFrame);
			delete(r);
		}

		emit newFrame(destFrame);
		emit frameHasHeader(infoList.at(currentFileIndex).hasHeader);

		QSize s = QSize(frameWidth, frameHeight);
		emit frameSize(s);
	}

	return *destFrame;
}

void SequenceReader::setSizeType(ImageSizes s)
{
	rawWidth = rawImageWidth[s];
	rawHeight = rawImageHeight[s];

	//if we have any files, the info will have changed
	if (fileList.size() > 0)
		buildFileInfo();

	readFrame(currentFrameNum);
}

//the dimensions of the frame are changed
void SequenceReader::setCustomSize(int width, int height)
{
	rawImageWidth[SizeCustom] = width;
	rawImageHeight[SizeCustom] = height;
}

int SequenceReader::getCustomWidth() {
	return rawImageWidth[SizeCustom];
}

int SequenceReader::getCustomHeight() {
	return rawImageHeight[SizeCustom];
}

bool SequenceReader::getFrameHasHeader(void)
{
	bool hasHeader = false;

	if (infoList.size() > 0)
		hasHeader = infoList.at(currentFileIndex).hasHeader;

	return hasHeader;
}
