/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include "view.h"

#include "chromaResample.h"
#include "colourSpace.h"

#include <QtGui>

#include <math.h>

View::View(QWidget *parent) :
	QGraphicsView(parent)
{
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	setDragMode(QGraphicsView::ScrollHandDrag);

	zoom=0.1f;
	rotation=0;
	setupMatrix();

	//YUV image display defaults
	yuvDisplayMode = YUVdisplayColour;
	yuvImageNativeBitDepth = 10;
	yuvImageBitDepth = 10;
	yuvImageRGBScaling = true;
	yuvImageColourMatrix = ColourSpace::HDTVtoRGB;

	inChroma = RawFrame::Cr422;
	inFrame = new RawFrame(0, 0, inChroma);
	frame444 = new RawFrame(0, 0, RawFrame::Cr444);
	frameRGB = new RawFrame(0, 0, RawFrame::CrRGB);

	scene = new QGraphicsScene;
	setScene(scene);

	QBrush brush = QBrush(QColor(32,32,32), Qt::SolidPattern);
	setBackgroundBrush(brush);
}

void View::setupMatrix()
{
	QMatrix matrix;
	matrix.scale(zoom, zoom);
	matrix.rotate(rotation);

	setMatrix(matrix);
}

void View::showFrame(const RawFrame *f)
{
	inFrame = (RawFrame *)f;
	processImage(processResample);
}

//erase the input and display data to ensure that the displayed image
//is removed when opening the next frame
void View::invalidateFrame()
{
	if(inFrame->chroma == RawFrame::CrRGB) {
		inFrame->r.assign(0);
		inFrame->g.assign(0);
		inFrame->b.assign(0);
	} else {
		//this will result in a illegal green frame - ie something is wrong
		inFrame->luma.assign(0);
		inFrame->cb.assign(0);
		inFrame->cr.assign(0);
	}
}

void View::setZoom(float value)
{
	if(value !=zoom) {
		zoom = value;
		setupMatrix();
		emit(zoomChanged(zoom));
	}
}

float View::getZoom(void)
{
	//this assume sthat the transformation matrix m11 and m22 are the same, so that there is
	//the same zoom ratio in x/y
	return matrix().m11();
}

int View::getRotation(void)
{
	return(rotation);
}

//change the mode used to display the YCbCr image (colour / Yonly / planar)
void View::setYUVdisplayMode(YUVdisplayMode mode){
if(mode != yuvDisplayMode) {
		yuvDisplayMode = mode;
		processImage(processColour);
	}
}

//set the scaling used when converting from YCbCr to RGB, and reprocess the input
void View::setYUVRGBScaling(bool scaled) {
	if(scaled != yuvImageRGBScaling) {
		yuvImageRGBScaling = scaled;
		processImage(processColour);
	}
}

//the GUI can request a particular bit depth to use for display
void View::setYUVbitDepth(int bits)
{
if (bits!= yuvImageBitDepth) {
		yuvImageBitDepth = bits;
		processImage(processColour);
	}
}

//the original file can have a native bit depth which may be 0 if unknown
void View::setYUVnativeBitDepth(int bits)
{
	yuvImageNativeBitDepth = bits;
}

//set the colour matrix type and reprocess the image
void View::setYUVColourMatrix(ColourSpace::MatrixType type)
{
	if(type != yuvImageColourMatrix) {
		yuvImageColourMatrix = type;
		processImage(processColour);
	}
}

void View::processImage(const ProcessType type)
{
	QImage displayImage(inFrame->luma.width(), inFrame->luma.height(), QImage::Format_RGB32);

	QImage *planarY = NULL;
	QImage *planarCb = NULL;
	QImage *planarCr = NULL;

	//these are zero sized as the process_ext methods are used on external data
	ChromaResample chromaResample(0, 0, RawFrame::Cr444);
	ColourSpace colourSpace(0, 0, RawFrame::CrRGB);

	//default to the native bit depth of the image
        int depth = yuvImageNativeBitDepth;

	//if the bit depth cannot be determined, take the one from the GUI
	if(depth == 0) depth = yuvImageBitDepth;

	switch(type) {

		case processResample:
		//check that the 444 frame is the right size
		if(frame444->luma.width() != inFrame->luma.width() ||
			frame444->luma.height() != inFrame->luma.height()) {
			delete frame444;
			frame444 = new RawFrame(inFrame->luma.width(), inFrame->luma.height(), RawFrame::Cr444);
		}

		if(inFrame->chroma != RawFrame::CrRGB) {
			chromaResample.process_ext(*inFrame, *frame444);
		}
		else {
			//copying - inefficient but useful for later
			frame444->r = inFrame->r;
			frame444->g = inFrame->g;
			frame444->b = inFrame->b;
		}

		case processColour:
		{
			//check that the RGB frame is the right size
			if(frameRGB->luma.width() != frame444->luma.width() ||
				frameRGB->luma.height() != frame444->luma.height()) {
				delete frameRGB;
				frameRGB = new RawFrame(inFrame->luma.width(), inFrame->luma.height(), RawFrame::CrRGB);
			}

			if(inFrame->chroma != RawFrame::CrRGB) {
				bool YOnly = false;
				if(inFrame->chroma == RawFrame::YOnly || yuvDisplayMode == YUVdisplayYOnly || yuvDisplayMode == YUVdisplayPlanar) YOnly = true;
				colourSpace.process_ext(*frame444, *frameRGB, yuvImageColourMatrix, yuvImageRGBScaling, YOnly, depth);
			}
			else {
				//more inefficient copying
				frameRGB->r = frame444->r;
				frameRGB->g = frame444->g;
				frameRGB->b = frame444->b;
			}
		}

		case processDisplay:
		{
			//copy the data to a QImage
			for(int y=0; y<frameRGB->r.height(); y++) {
				for(int x=0; x<frameRGB->r.width(); x++) {

					int R = frameRGB->r[y][x] >> (depth - 8);
					int G = frameRGB->g[y][x] >> (depth - 8);
					int B = frameRGB->b[y][x] >> (depth - 8);

					int pixel = 0xff << 24 | R << 16 | G << 8 | B;

					displayImage.setPixel(x, y, pixel);
				}
			}

			//generate seperate QImages for the chrominance if we are doing planar display
			if(yuvDisplayMode == YUVdisplayPlanar && inFrame->chroma != RawFrame::YOnly) {
				planarY = new QImage(inFrame->luma.width(), inFrame->luma.height(), QImage::Format_RGB32);
				planarCb = new QImage(inFrame->cb.width(), inFrame->cr.height(), QImage::Format_RGB32);
				planarCr = new QImage(inFrame->cr.width(), inFrame->cr.height(), QImage::Format_RGB32);

				//Y
				for(int y=0; y<inFrame->luma.height(); y++) {
					for(int x=0; x<inFrame->luma.width(); x++) {

						int C = inFrame->luma[y][x] >> (depth - 8);
						int pixel = C << 16 | C << 8 | C;
						planarY->setPixel(x, y, pixel);
					}
				}

				//Cb
				for(int y=0; y<inFrame->cb.height(); y++) {
					for(int x=0; x<inFrame->cb.width(); x++) {

						int C = inFrame->cb[y][x] >> (depth - 8);
						int pixel = C << 16 | C << 8 | C;
						planarCb->setPixel(x, y, pixel);
					}
				}

				//Cr
				for(int y=0; y<inFrame->cr.height(); y++) {
					for(int x=0; x<inFrame->cr.width(); x++) {

						int C = inFrame->cr[y][x] >> (depth - 8);
						int pixel = C << 16 | C << 8 | C;
						planarCr->setPixel(x, y, pixel);
					}
				}

			}

		}

		case processScene:
		{
			//Put the image on the graphics scene, and set the graphics view

			if(scene == NULL)
			scene = new QGraphicsScene;

			//remove all items from the scene
			foreach (QGraphicsItem *item, scene->items()) {
				scene->removeItem(item);
				delete item;
			}

			//add the planar format chroma if required
			if(yuvDisplayMode == YUVdisplayPlanar && inFrame->chroma != RawFrame::YOnly) {

				//Y at the top
				QGraphicsPixmapItem* yItem = scene->addPixmap(QPixmap::fromImage(*planarY));
				yItem->setCursor(Qt::ArrowCursor);

				//Cb to the left
				QGraphicsPixmapItem *cbItem = scene->addPixmap(QPixmap::fromImage(*planarCb));
				cbItem->setPos(QPoint(0, inFrame->luma.height()));
				cbItem->setCursor(Qt::ArrowCursor);

				//Cr to the right or below
				if(inFrame->chroma == RawFrame::Cr444 || inFrame->chroma == RawFrame::CrRGB) {
					//the resulting image is three times the original height
					QGraphicsPixmapItem *crItem = scene->addPixmap(QPixmap::fromImage(*planarCr));
					crItem->setPos(QPoint(0, inFrame->luma.height()*2));
					crItem->setCursor(Qt::ArrowCursor);
					scene->setSceneRect(0, 0, inFrame->luma.width(), inFrame->luma.height() * 3);
				}
				else {
					//we can fit the two pieces of chroma next to each other
					QGraphicsPixmapItem *crItem = scene->addPixmap(QPixmap::fromImage(*planarCr));
					crItem->setPos(QPoint(inFrame->luma.width()/2, inFrame->luma.height()));
					crItem->setCursor(Qt::ArrowCursor);
					scene->setSceneRect(0, 0, inFrame->luma.width(), inFrame->luma.height() + inFrame->cb.height());
				}

			}
			else {
				//the scene only encompasses one colour or YOnly pixmap
				QGraphicsPixmapItem* pixmapItem = scene->addPixmap(QPixmap::fromImage(displayImage));
				pixmapItem->setCursor(Qt::ArrowCursor);
				scene->setSceneRect(0, 0, inFrame->luma.width(), inFrame->luma.height());
			}

			setScene(scene);

		}
	}

	//these will only have been used if displaying planar format
	delete planarY;
	delete planarCb;
	delete planarCr;
}

//make the mouse wheel zoom in and out over the pointer position
void View::wheelEvent(QWheelEvent * wheelEvent)
{
	float newZoom;

	if(wheelEvent->delta()> 0)
	newZoom = zoom * 2;
	else
	newZoom = zoom / 2;

	//prevent extreme zooming out
	if(newZoom < 0.01)
	return;

	//resize about the mouse position
	setTransformationAnchor(AnchorUnderMouse);
	setZoom(newZoom);
	setTransformationAnchor(AnchorViewCenter);
}

//double click shows pixel info
void View::mouseDoubleClickEvent(QMouseEvent *e)
{
	int mousex=e->x();
	int mousey=e->y();

	//this is very neat. It returns the click position on the displayed pixmap
	//accounting for the zoom/rotation of the scene
	QPoint viewPoint(mousex, mousey);
	QPointF scenePoint = mapToScene(viewPoint);

	int pixelx = (int)scenePoint.x();
	int pixely = (int)scenePoint.y();

	//it is possible to zoom out then double click outside the scene.
	//do not try to use these coordinates to index our arrays of pixels
	if(pixelx < 0 || pixelx> sceneRect().width()) return;
	if(pixely < 0 || pixely> sceneRect().height()) return;

	QString pixelInfo;

	if(yuvDisplayMode != YUVdisplayPlanar) {

		if(inFrame->chroma != RawFrame::CrRGB) {
			//YUV and RGB colour display
			pixelInfo = QString("Pixel at (%1,%2)  RGB=[%3,%4,%5]  YCbCr=[%6,%7,%8]")
			.arg(pixelx) .arg(pixely)
			.arg(frameRGB->r[pixely][pixelx]) .arg(frameRGB->g[pixely][pixelx]) .arg(frameRGB->b[pixely][pixelx])
			.arg(frame444->luma[pixely][pixelx]) .arg(frame444->cb[pixely][pixelx]) .arg(frame444->cr[pixely][pixelx]);
		}
		else
		{
			pixelInfo = QString("Pixel at (%1,%2)  RGB=[%3,%4,%5]")
			.arg(pixelx) .arg(pixely)
			.arg(frameRGB->r[pixely][pixelx]) .arg(frameRGB->g[pixely][pixelx]) .arg(frameRGB->b[pixely][pixelx]);
		}
	}
	else {

		if(inFrame->chroma == RawFrame::Cr444 || inFrame->chroma == RawFrame::CrRGB) {

			//chroma stacked one above the other

			if(pixely < inFrame->luma.height()) {
				//luminance
				pixelInfo = QString("Pixel at (%1,%2)  RGB=[%3,%4,%5]  Y=[%6]")
				.arg(pixelx) .arg(pixely)
				.arg(frameRGB->r[pixely][pixelx]) .arg(frameRGB->g[pixely][pixelx]) .arg(frameRGB->b[pixely][pixelx])
				.arg(inFrame->luma[pixely][pixelx]);
			}
			else if(pixely < inFrame->luma.height() * 2) {
				//Cb
				int cby = pixely - inFrame->luma.height();
				pixelInfo = QString("Cb Pixel at (%1,%2) Cb=[%3]")
				.arg(pixelx) .arg(cby)
				.arg(inFrame->cb[cby][pixelx]);
			}
			else {
				//Cr
				int cry = pixely - (inFrame->luma.height() * 2);
				pixelInfo = QString("Cr Pixel at (%1,%2) Cr=[%3]")
				.arg(pixelx) .arg(cry)
				.arg(inFrame->cr[cry][pixelx]);
			}
		}
		else {
			//chroma side by side
			if(pixely < inFrame->luma.height()) {
				//luminance
				pixelInfo = QString("Pixel at (%1,%2)  RGB=[%3,%4,%5]  Y=[%6]")
				.arg(pixelx) .arg(pixely)
				.arg(frameRGB->r[pixely][pixelx]) .arg(frameRGB->g[pixely][pixelx]) .arg(frameRGB->b[pixely][pixelx])
				.arg(inFrame->luma[pixely][pixelx]);
			}
			else if (pixelx < (inFrame->luma.width() / 2)) {
				//Cb
				if(pixelx> inFrame->cb.width()) return; //cannot look in 4:1:1 where there is no data
				int cby = pixely - inFrame->luma.height();
				pixelInfo = QString("Cb Pixel at (%1,%2) Cb=[%3]")
				.arg(pixelx) .arg(cby)
				.arg(inFrame->cb[cby][pixelx]);
			}
			else {
				//Cr
				int crx = pixelx - (inFrame->luma.width() / 2);
				if(crx> inFrame->cr.width()) return; //cannot look in 4:1:1 where there is no data
				int cry = pixely - inFrame->luma.height();
				pixelInfo = QString("Cr Pixel at (%1,%2) Cr=[%3]")
				.arg(crx) .arg(cry)
				.arg(inFrame->cr[cry][crx]);
			}
		}
	}

	emit(pixelInfoMessage(pixelInfo, 0));
}
