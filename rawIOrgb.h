/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __RAWIORGB_H
#define __RAWIORGB_H

#include <stdio.h>
#include "rawFrameIO.h"

class RawWriterrgb : public RawWriter {
public:
	RawWriterrgb(FILE* fd) :
		outfile(fd)
	{
	};

	/* write out a frame without altering the frame. */
	void write(const RawFrame&) const;

private:
	FILE *outfile;
};

class RawReaderrgb : public RawReader {
public:
	RawReaderrgb(FILE* fd) :
		infile(fd)
	{
	};
	/* read a frame into the reference provided */
	RawFrame& read(RawFrame&);
	RawFrame& read();

private:
	FILE *infile;
};

#endif
