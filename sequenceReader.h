/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __SEQUENCEREADER_H
#define __SEQUENCEREADER_H

#include "rawFrame.h"
#include "rawFrameIO.h"

#include <vector>

#include <QStringList>
#include <QObject>

#include <QtGui>

class SequenceReader : public QObject
{
	Q_OBJECT
public:
	SequenceReader();

	//Values of this type are stored in the settings, so if you need to extend this enum please only add to the end,
	//or settings and program values may get out of step.
		typedef enum _RawType {type_v216,
	                        type_v210,
	                        type_yv12,
	                        type_i420,
	                        type_uyvy,
	                        type_pgm,
	                        type_ppm,
	                        type_uy16,
	                        type_yy16,
	                        type_yyy8,
	                        type_16P4,
	                        type_16P2,
	                        type_16P0,
	                        type_8P2,
	                        type_sgi,
                          type_leader_frm,
	                        type_unknown,
	                        type_auto,
	                        type_rgb,
	                        type_rgba} RawType;

	enum ImageSizes {SizeQSIF=0, SizeQCIF, SizeSIF, SizeCIF, Size4SIF, Size4CIF, SizeSD480, SizeSD576, SizeHD720, SizeHD1080, Size2K, Size4K, SizeSHV, SizeCustom, SizeUnknown, NumSizes=SizeUnknown};

	int getSequenceLength(void);
	int getFirstFrameNum(void);
	int getLastFrameNum(void);
	int getCurrentFrameNum(void);

	int getFrameWidth(void) {return frameWidth;}
	int getFrameHeight(void) {return frameHeight;}
	int getFrameDepth(void) {return frameDepth;}
	bool getFrameHasHeader(void);

	int getCustomWidth();
	int getCustomHeight();
	RawFrame& readFrame(int frameNum);

signals:
	void newFrame(const RawFrame*);
	void frameFileName(const QString &);
	void frameFormat(const QString &);
	void frameSize(QSize &);
	void sequencePosition(const QString&, int, int, int, int);
	void frameHasHeader(bool);
	void frameBitDepth(int);

public slots:
	int setDirectoryOfFiles(const QString &dir);
	int setFileNames(const QStringList &);
	void setSizeType(ImageSizes s);
	void setCustomSize(int width, int height);
	void setRawFormat(RawType);
	void nextInSequence();
	void prevInSequence();

private slots:

private:

	int jumpInSequence(int);

	typedef struct { //information about each file in the sequence
		RawFrame::Chroma chroma;
		RawType rawType;
		int bitDepth;
		bool hasHeader;
		int bytesPerFrame;
		int framesInFile;
		int firstFrameNum;
		int lastFrameNum;
	} sequenceFileInfo;

	int rawImageWidth[NumSizes];
	int rawImageHeight[NumSizes];

	void buildFileInfo(void); //look through the file list and gather information on each video file
	int bytesPerFrame(RawType); //turn a file extension into a number of bytes per frame for the current picture size
	bool hasHeader(RawType); //does the file have a header which allows file information to be read?
	int bitDepth(RawType); //can the pixel bit depth be determined for a file from the file extension?
	RawFrame::Chroma chromaType(RawType); //chroma type we read into, determined by file extension
	RawType rawType(QString &extension); //enumerated version of file extension

	int fileIndexForFrameNum(int frameNum); //look through the file info list for the file containing a particular frame number

	RawType frameType; //the user interface may force the raw yuv file type regardless of the extension
	int frameDepth; //the bit depth for the frame if is is known / deduced, or 0 otherwise
	int frameWidth; //the actual frame dimensions, either read from the header or taken from rawWidth/rawHeight
	int frameHeight;

	int rawWidth; //dimensions for raw files, where the frame size is unknown
	int rawHeight;

	int firstFrame; //first frame number in the sequence
	int lastFrame; //last frame number in the sequence
	int currentFrameNum; //current
	int currentFileIndex; //current file in the list of files to display (each may contain more than one frame)
	FILE *filePtr; //the currently open file
	QStringList fileList; //list of filenames
	std::vector<sequenceFileInfo> infoList; //list of info about each file of frames
	RawFrame *destFrame; //the destination frame for reading into
};

#endif
