/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <assert.h>
#include "rawIOuyvy.h"

/* this could be improved by reading a larger chunk and then converting
 * the endianness suitably.  Reading / writing converts the chroma values
 * to offset binary ycbcr. */
RawFrame& RawReaderuyvy::read(RawFrame &f)
{
	assert(f.chroma == RawFrame::Cr422);

	size_t linesRead;
	size_t lineLen = f.luma.width() * 2;
	unsigned char *inLine = new unsigned char[lineLen];

	for (int y = 0; y < f.luma.height(); y++) {

		linesRead = fread(inLine, lineLen, 1, infile);

		for (int in = 0, xy = 0, xc = 0; xy < f.luma.width(); in+=4, xy += 2,
		                                                      xc++) {
			/* Cb Y Cr Y */
			f.cb[y][xc] = inLine[in+0];
			f.luma[y][xy] = inLine[in+1];
			f.cr[y][xc] = inLine[in+2];
			f.luma[y][xy+1] = inLine[in+3];
		}

		if (linesRead < 1)
			goto exit;
	}

	exit:

	//if (feof (infile))
	//    throw /* some eof/shortread exception */ ;
	free(inLine);
	return f;
}

RawFrame& RawReaderuyvy::read()
{
	throw /* unknown framesize */;
}

void RawWriteruyvy::write(const RawFrame& f) const
{
	assert(f.chroma == RawFrame::Cr422);
	for (int y = 0; y < f.luma.height(); y++)
		for (int xy = 0, xc = 0; xy < f.luma.width(); xy += 2, xc++) {
			/* Cb Y Cr Y */
			fputc(f.cb[y][xc], outfile);
			fputc(f.luma[y][xy], outfile);
			fputc(f.cr[y][xc], outfile);
			fputc(f.luma[y][xy+1], outfile);
		}
}
