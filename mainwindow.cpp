/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include "mainwindow.h"
#include "view.h"
#include "sequenceReader.h"
#include "customSizeDialog.h"

#include <QtGui>
#include <QtGlobal>

MainWindow::MainWindow() : assistantClientStarted(false)
{
	//central widget is the 'view'
	view = new View();
	view->setFrameStyle(QFrame::NoFrame);
	setCentralWidget(view);
	setWindowTitle("ImageViewer");

	//set up menus etc
	createActions();
	createStatusBar();
	createToolBar();
	setStatusBar(myStatusBar);
	addToolBar(Qt::TopToolBarArea, myToolBar);
  assistantClientProcess = new QProcess(this);

	//when a pixel is clicked on in the view, print info about it
	connect(view, SIGNAL(pixelInfoMessage(const QString &, int)), myStatusBar, SLOT(showMessage(const QString &, int)));

	//when the mouse wheel changes to zoom, update the menus and status bar
	connect(view, SIGNAL(zoomChanged(float)), this, SLOT(viewImageZoomChanged(float)));

	//each new frame that the sequence reader reads emits these signals
	connect(&reader, SIGNAL(newFrame(const RawFrame*)), view, SLOT(showFrame(const RawFrame *)));
	connect(&reader, SIGNAL(frameFileName(const QString &)), fileNameLabel, SLOT(setText(const QString &)));
	connect(&reader, SIGNAL(frameFormat(const QString &)), fileFormatLabel, SLOT(setText(const QString &)));
	connect(&reader, SIGNAL(frameSize(QSize &)), this, SLOT(processSize(QSize &)));
	connect(&reader, SIGNAL(sequencePosition(const QString&, int, int, int, int)), this, SLOT(processSequencePosition(const QString&, int, int, int, int)));
	connect(&reader, SIGNAL(frameHasHeader(bool)), this, SLOT(processHasHeader(bool)));
	connect(&reader, SIGNAL(frameBitDepth(int)), this, SLOT(processBitDepth(int)));

	//load the settings for this application
	QCoreApplication::setOrganizationName("BBCResearch");
	QCoreApplication::setOrganizationDomain("rd.bbc.co.uk");
	QCoreApplication::setApplicationName("RawImageViewer");
	readSettings();

	//if there are any parameters, pass these to the view as filenames
	if (qApp->arguments().count() > 1) {
		QStringList fileNames = qApp->arguments();
		fileNames.removeAt(0); //delete the name of the executable
		reader.setFileNames(fileNames);
		reader.readFrame(reader.getCurrentFrameNum());
	}
}

//initialise the Qt assistant, pointing it to our help content
void MainWindow::initializeAssistant()
{
//set the path to the documentation files
QStringList arguments;

  arguments << QLatin1String("-collectionFile") << QString(DOCDIR) + QString("imageviewer.qhc") << QLatin1String("-enableRemoteControl");

	//launch the assistant
	//
	if (false == assistantClientStarted)
	{
    QString app = QLibraryInfo::location(QLibraryInfo::BinariesPath)
                 + QLatin1String("/assistant");
             assistantClientProcess->start(app, arguments);
             if (!assistantClientProcess->waitForStarted()) {
                 QMessageBox::critical(this, tr("Remote Control"),
                     tr("Could not start Qt Assistant from %1.").arg(app));
                 return;
             }
             connect(assistantClientProcess, SIGNAL(finished(int,QProcess::ExitStatus)),
                                                                this, SLOT(helpViewerClosed()));
	}
}

//slot to open the assistant when requested from the help menu
void MainWindow::assistant()
{
  initializeAssistant();
  QByteArray ba;
	ba.append("SetSource qthelp://bbcrd/imageviewer_v1.0/index.html\n");
	assistantClientProcess->write(ba);
}

//open a sequence of files
void MainWindow::openSeq()
{
	QSettings settings;
	QStringList fileNames = QFileDialog::getOpenFileNames(
                                      this,
                                      tr("Open Files"),
                                      settings.value("fileopen/directory").toString(),
                                      tr("Raw YUV Images (*.v210 *.uy16 *.v216 *.yv12 *.i420 *.uyvy *.yuv *.16P4 *.16P2 *.16P0 *.8P2);;Portable Images (*.pgm *.ppm);;SGI Files (*.sgi);;Any Files(*)"));

	if (fileNames.size() > 0) {
		QFileInfo fi(fileNames.at(0));
		settings.setValue("fileopen/directory", fi.absolutePath());
			if (!fileNames.isEmpty()) {
			recentFiles.removeAll(fileNames.at(0));
			recentFiles.prepend(fileNames.at(0));
			updateRecentFileActions();

			reader.setFileNames(fileNames);
			reader.readFrame(reader.getCurrentFrameNum());

		}
	}
}

//open a directory of files
void MainWindow::openDirectory()
{
	QSettings settings;
	QString fileName = QFileDialog::getExistingDirectory(
			this,
			tr("Open Directory of images"),
			settings.value("fileopen/directory").toString(),
			QFileDialog::ShowDirsOnly);

	if (fileName.isEmpty() == false) {
		QFileInfo fi(fileName);
		settings.setValue("fileopen/directory", fi.absolutePath());
		reader.setDirectoryOfFiles(fileName);
		reader.readFrame(reader.getCurrentFrameNum());
	}
}

//slot to handle selection of a recent file from the file menu
void MainWindow::openRecentFile()
{
	QAction *action = qobject_cast<QAction *>(sender());
	if (action) {
		QStringList list = QStringList(action->data().toString());
		recentFiles.removeAll(list.at(0));
		recentFiles.prepend(list.at(0));
		updateRecentFileActions();
		reader.setFileNames(list);
		reader.readFrame(reader.getCurrentFrameNum());
	}
}

//populate file menu actions with recent file list
void MainWindow::updateRecentFileActions()
{
	QMutableStringListIterator i(recentFiles);
	while (i.hasNext()) {
		if (!QFile::exists(i.next()))
			i.remove();
	}

	for (int j = 0; j < MaxRecentFiles; ++j) {
		if (j < recentFiles.count()) {
			QString text = tr("&%1 %2") .arg(j + 1) .arg(recentFiles[j]);
			recentFileAct[j]->setText(text);
			recentFileAct[j]->setData(recentFiles[j]);
			recentFileAct[j]->setVisible(true);
		} else {
			recentFileAct[j]->setVisible(false);
		}
	}

	recentFileSeperatorAct->setVisible(!recentFiles.isEmpty());
}

//status bar
void MainWindow::createStatusBar(void)
{
	myStatusBar = new QStatusBar;

	fileFormatLabel = new QLabel("Source Format", this);
	fileFormatLabel->setFrameStyle(QFrame::Panel | QFrame::Sunken);

	fileSizeLabel = new QLabel("Image Size Label", this);
	fileSizeLabel->setFrameStyle(QFrame::Panel | QFrame::Sunken);

	fileZoomLabel = new QLabel("Image Zoom Label", this);
	fileZoomLabel->setFrameStyle(QFrame::Panel | QFrame::Sunken);

	fileNameLabel = new QLabel("Filename", this);
	fileNameLabel->setFrameStyle(QFrame::Panel | QFrame::Sunken);

	myStatusBar->insertPermanentWidget(0, fileFormatLabel, 0);
	myStatusBar->insertPermanentWidget(1, fileSizeLabel, 0);
	myStatusBar->insertPermanentWidget(2, fileZoomLabel, 0);
	myStatusBar->insertPermanentWidget(3, fileNameLabel, 0);
}

//tool bar
void MainWindow::createToolBar(void)
{
	myToolBar = new QToolBar("Navigation", this);
	connect(myToolBar, SIGNAL(actionTriggered(QAction *)), this, SLOT(jumpToFrameRelative(QAction *)));
	QAction *a;

	QIcon firstIcon(":icons/first.svg");
	a = myToolBar->addAction(firstIcon, tr("First Frame"), this, SLOT(jumpToFirstFrame()));
	a->setData(0);

	QIcon back1000Icon(":icons/back1000.svg");
	a = myToolBar->addAction(back1000Icon, tr("Back 1000"));
	a->setData(-1000);

	QIcon back100Icon(":icons/back100.svg");
	a = myToolBar->addAction(back100Icon, tr("Back 100"));
	a->setData(-100);

	QIcon back10Icon(":icons/back10.svg");
	a = myToolBar->addAction(back10Icon, tr("Back 10"));
	a->setData(-10);

	QIcon back1Icon(":icons/back1.svg");
	a = myToolBar->addAction(back1Icon, tr("Back 1"));
	a->setData(-1);

	jumpToFrameSpin = new QSpinBox(this);
	jumpToFrameSpin->setMinimum(0);
	jumpToFrameSpin->setMaximum(0);
	jumpToFrameSpin->setMinimumWidth(100);
	myToolBar->addWidget(jumpToFrameSpin);
	connect(jumpToFrameSpin, SIGNAL(editingFinished()), this, SLOT(jumpToFrameAbsolute()));

	QIcon fwd1Icon(":icons/fwd1.svg");
	a = myToolBar->addAction(fwd1Icon, tr("Forward 1"));
	a->setData(1);

	QIcon fwd10Icon(":icons/fwd10.svg");
	a = myToolBar->addAction(fwd10Icon, tr("Forward 10"));
	a->setData(10);

	QIcon fwd100Icon(":icons/fwd100.svg");
	a = myToolBar->addAction(fwd100Icon, tr("Forward 100"));
	a->setData(100);

	QIcon fwd1000Icon(":icons/fwd1000.svg");
	a = myToolBar->addAction(fwd1000Icon, tr("Forward 1000"));
	a->setData(1000);

	QIcon lastIcon(":icons/last.svg");
	a = myToolBar->addAction(lastIcon, tr("Last Frame"), this, SLOT(jumpToLastFrame()));
	a->setData(0);
}

//about box
void MainWindow::about()
{
	QMessageBox::about(this, tr("Image Viewer"),
	                   tr("Image Viewer for raw YCbCr and\n"
	                   "PGM / PPM image formats\n"
	                   "Version 1.3\n\n"
	                   "(c) BBC Research & Development 2009\n"
	                   "    Jonathan Rosser\n"
	                   "    David Flynn\n\n"
	                   "Win32 binaries: http://diracvideo.org/download/imageviewer\n"
	                   "Source code: http://diracvideo.org/git/imageviewer.git"));
}

//generate actions for menus and keypress handlers
void MainWindow::createActions()
{

//add a keyboard only shortcut not appearing on a menu
#define ADD_SHORTCUT(str, key, target, fn) \
	do { QAction *a = new QAction(str, this); \
		a->setShortcut(tr(key)); \
		addAction(a); \
		connect(a, SIGNAL(triggered()), target, SLOT(fn())); \
	} while(0);

//add an action to an actiongroup in a menu
#define ADD_MENUGROUPITEM(str, data, menu, group, checkable, checked) \
	do { QAction *a = new QAction(str, this); \
		a->setData(data); \
		a->setCheckable(checkable); \
		a->setChecked(checked); \
		group->addAction(a); \
		menu->addAction(a); \
	} while(0);

	/////////////
	// FILE MENU
	/////////////
	QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addAction("Open...", this, SLOT(openSeq()), tr("Ctrl+O"));
	fileMenu->addAction(QString("Open Dir..."), this, SLOT(openDirectory()));
	fileMenu->addSeparator();

	//recent files
	for (int i = 0; i < MaxRecentFiles; ++i) {
		recentFileAct[i] = fileMenu->addAction("", this, SLOT(openRecentFile()));
		recentFileAct[i]->setVisible(false);
	}
	recentFileSeperatorAct = fileMenu->addSeparator();

	fileMenu->addAction("E&xit", this, SLOT(close()), tr("Ctrl+Q"));

	/////////////
	// IMAGE MENU
	/////////////
	QMenu *imageMenu = menuBar()->addMenu(tr("&Image"));

	//Image Format
	imageFormatMenu = imageMenu->addMenu(tr("YCbCr Image Format"));
	imageFormatGroup = new QActionGroup(this);
	ADD_MENUGROUPITEM("Auto", SequenceReader::type_auto, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("I420", SequenceReader::type_i420, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("YV12", SequenceReader::type_yv12, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("UYVY", SequenceReader::type_uyvy, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("V210", SequenceReader::type_v210, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("V216", SequenceReader::type_v216, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("UY16", SequenceReader::type_uy16, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("YY16", SequenceReader::type_yy16, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("YYY8", SequenceReader::type_yyy8, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("16P4", SequenceReader::type_16P4, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("16P2", SequenceReader::type_16P2, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("16P0", SequenceReader::type_16P0, imageFormatMenu, imageFormatGroup, true, false);
	ADD_MENUGROUPITEM("8P2",  SequenceReader::type_8P2,  imageFormatMenu, imageFormatGroup, true, false);
	connect(imageFormatGroup, SIGNAL(triggered(QAction *)), this, SLOT(setYUVimageFormat(QAction *)));

	//Image Size
	imageSizeMenu = imageMenu->addMenu(tr("YCbCr Image Size"));
	imageSizeGroup = new QActionGroup(this);
	ADD_MENUGROUPITEM("QSIF : 176x120", SequenceReader::SizeQSIF, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("QCIF : 176x144", SequenceReader::SizeQCIF, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("SIF : 352x240", SequenceReader::SizeSIF, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("CIF : 352x288", SequenceReader::SizeCIF, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("4SIF : 704x480", SequenceReader::Size4SIF, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("4SIF : 704x576", SequenceReader::Size4CIF, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("SD480 : 720x480", SequenceReader::SizeSD480, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("SD576 : 720x576", SequenceReader::SizeSD576, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("HD720 : 1280x720", SequenceReader::SizeHD720, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("HD1080 : 1920x1080", SequenceReader::SizeHD1080, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("2K : 2048x1556", SequenceReader::Size2K, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("4K : 4096x3112", SequenceReader::Size4K, imageSizeMenu, imageSizeGroup, true, false);
	ADD_MENUGROUPITEM("SHV : 7680x4320", SequenceReader::SizeSHV, imageSizeMenu, imageSizeGroup, true, false);
	imageSizeMenu->addSeparator();
	connect(imageSizeGroup, SIGNAL(triggered(QAction *)), this, SLOT(setPresetSize(QAction *)));

	imageSizeSetCustomAct = imageSizeMenu->addAction("Custom...", this, SLOT(setCustomSize(bool)));
	imageSizeSetCustomAct->setCheckable(true);

	//Image bit depth menu
	imageDepthMenu = imageMenu->addMenu(tr("Image Bit Depth"));
	imageDepthGroup = new QActionGroup(this);
	ADD_MENUGROUPITEM("8 Bit", 8, imageDepthMenu, imageDepthGroup, true, false);
	ADD_MENUGROUPITEM("10 Bit", 10, imageDepthMenu, imageDepthGroup, true, true);
	ADD_MENUGROUPITEM("12 Bit", 12, imageDepthMenu, imageDepthGroup, true, false);
	ADD_MENUGROUPITEM("14 Bit", 14, imageDepthMenu, imageDepthGroup, true, false);
	ADD_MENUGROUPITEM("16 Bit", 16, imageDepthMenu, imageDepthGroup, true, false);
	connect(imageDepthGroup, SIGNAL(triggered(QAction *)), this, SLOT(setYUVimageDepth(QAction *)));

	////////////
	// VIEW MENU
	////////////
	QMenu *viewMenu = menuBar()->addMenu(tr("&View"));

	//YUV display mode action group
	YUVModeGroup = new QActionGroup(this);
	ADD_MENUGROUPITEM("YUV in colour", View::YUVdisplayColour, viewMenu, YUVModeGroup, true, true);
	ADD_MENUGROUPITEM("YUV Y-Only", View::YUVdisplayYOnly, viewMenu, YUVModeGroup, true, false);
	ADD_MENUGROUPITEM("YUV in planar", View::YUVdisplayPlanar, viewMenu, YUVModeGroup, true, false);
	connect(YUVModeGroup, SIGNAL(triggered(QAction *)), this, SLOT(setYUVdisplayMode(QAction *)));
	viewMenu->addSeparator();

	//rgb scaling action group
	rgbScalingGroup = new QActionGroup(this);
	ADD_MENUGROUPITEM("Scaled YCbCr to RGB", true, viewMenu, rgbScalingGroup, true, true);
	ADD_MENUGROUPITEM("Unscaled YCbCr to RGB", false, viewMenu, rgbScalingGroup, true, false);
	connect(rgbScalingGroup, SIGNAL(triggered(QAction *)), this, SLOT(setRGBScaling(QAction *)));
	viewMenu->addSeparator();

	//colour matrix group
	matrixGroup = new QActionGroup(this);
	ADD_MENUGROUPITEM("Use HDTV Matrix", ColourSpace::HDTVtoRGB, viewMenu, matrixGroup, true, true);
	ADD_MENUGROUPITEM("Use SDTV Matrix", ColourSpace::SDTVtoRGB, viewMenu, matrixGroup, true, false);
	connect(matrixGroup, SIGNAL(triggered(QAction *)), this, SLOT(setColourMatrix(QAction *)));
	viewMenu->addSeparator();

	//Zoom levels
	zoomGroup = new QActionGroup(this);
	ADD_MENUGROUPITEM("Zoom 25%", 0.25f, viewMenu, zoomGroup, true, false);
	ADD_MENUGROUPITEM("Zoom 50%", 0.50f, viewMenu, zoomGroup, true, false);
	ADD_MENUGROUPITEM("Zoom 100%", 1.0f, viewMenu, zoomGroup, true, true);
	ADD_MENUGROUPITEM("Zoom 200%", 2.0f, viewMenu, zoomGroup, true, false);
	ADD_MENUGROUPITEM("Zoom 400%", 4.0f, viewMenu, zoomGroup, true, false);
	ADD_MENUGROUPITEM("Zoom 800%", 8.0f, viewMenu, zoomGroup, true, false);
	connect(zoomGroup, SIGNAL(triggered(QAction *)), this, SLOT(setZoom(QAction *)));
	viewMenu->addSeparator();

	//scroll bar behaviour
	scrollBarGroup = new QActionGroup(this);
	ADD_MENUGROUPITEM("Scroll Bars on", Qt::ScrollBarAlwaysOn, viewMenu, scrollBarGroup, true, true);
	ADD_MENUGROUPITEM("Scroll Bars as required", Qt::ScrollBarAsNeeded, viewMenu, scrollBarGroup, true, false);
	ADD_MENUGROUPITEM("Scroll Bars off", Qt::ScrollBarAlwaysOff, viewMenu, scrollBarGroup, true, false);
	connect(scrollBarGroup, SIGNAL(triggered(QAction *)), this, SLOT(setScrollBar(QAction *)));
	viewMenu->addSeparator();

	toggleToolBarAct = new QAction("Show ToolBar", this);
	toggleToolBarAct->setShortcut(tr("Ctrl+T"));
	toggleToolBarAct->setCheckable(true);
	toggleToolBarAct->setChecked(true);
	addAction(toggleToolBarAct);
	connect(toggleToolBarAct, SIGNAL(triggered()), this, SLOT(toggleToolBar()));

	////////////
	// HELP MENU
	////////////
	QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

	helpMenu->addAction("Help Contents", this, SLOT(assistant()), tr("F1"));
	helpMenu->addAction("&About", this, SLOT(about()));
	helpMenu->addAction("About &Qt", qApp, SLOT(aboutQt()));

	//keyboard shortcuts that do not appear on menus
	ADD_SHORTCUT("View full screen", "Ctrl+F", this, toggleFullScreen);
	ADD_SHORTCUT("Escape full screen", "Escape", this, escapeFullScreen);
	ADD_SHORTCUT("Zoom In", "+", this, zoomIn);
	ADD_SHORTCUT("Zoom Out", "-", this, zoomOut);
	ADD_SHORTCUT("Next Frame", " ", &reader, nextInSequence);
	ADD_SHORTCUT("Next Frame", "n", &reader, nextInSequence);
	ADD_SHORTCUT("Prev Frame", "p", &reader, prevInSequence);
}

void MainWindow::setScrollBar(QAction *action)
{
	Qt::ScrollBarPolicy policy = (Qt::ScrollBarPolicy)action->data().toInt();

	view->setHorizontalScrollBarPolicy(policy);
	view->setVerticalScrollBarPolicy(policy);
}

//zoom menu slot
void MainWindow::setZoom(QAction *action)
{
	float zoom = (float)(action->data().toDouble());
	view->setZoom(zoom);
}

//rgb scaling menu slot
void MainWindow::setRGBScaling(QAction *action)
{
	view->setYUVRGBScaling(action->data().toBool());
}

//display mode menu slot
void MainWindow::setYUVdisplayMode(QAction *action)
{
	view->setYUVdisplayMode((View::YUVdisplayMode)action->data().toInt());
}

//colour matrix menu slot
void MainWindow::setColourMatrix(QAction *action)
{
	view->setYUVColourMatrix((ColourSpace::MatrixType)action->data().toInt());
}

void MainWindow::setYUVimageFormat(QAction *action)
{
	reader.setRawFormat((SequenceReader::RawType)action->data().toInt());
}

void MainWindow::setPresetSize(QAction *action)
{
	SequenceReader::ImageSizes size = (SequenceReader::ImageSizes)action->data().toInt();
	reader.setSizeType(size);
	updateRawImageSize(size);
}

//image size menu slot
void MainWindow::setCustomSize(bool)
{
	CustomSizeDialog sizeDialog("Custom Image Size",
	                            reader.getCustomWidth(),
	                            reader.getCustomHeight(), this);
	sizeDialog.exec();
	if (sizeDialog.result() == QDialog::Accepted) {

		reader.setCustomSize(sizeDialog.getWidth(), sizeDialog.getHeight());
		reader.setSizeType(SequenceReader::SizeCustom);
		updateRawImageSize(SequenceReader::SizeCustom);
		imageSizeSetCustomAct->setText(QString("Custom... [%1x%2]") .arg(reader.getCustomWidth()) .arg(reader.getCustomHeight()));
	}
}

//image depth menu slot
void MainWindow::setYUVimageDepth(QAction *action)
{
	view->setYUVbitDepth(action->data().toInt());
}

//slot to receive full screen toggle command
void MainWindow::toggleFullScreen()
{
	setFullScreen(!isFullScreen());
}

void MainWindow::escapeFullScreen()
{
	if (isFullScreen())
		setFullScreen(false);
}

void MainWindow::toggleToolBar()
{
	if (myToolBar->isVisible()) {
		myToolBar->hide();
		toggleToolBarAct->setChecked(false);
	}
	else {
		myToolBar->show();
		toggleToolBarAct->setChecked(true);
	}
}

//full screen toggle worker
void MainWindow::setFullScreen(bool fullscreen)
{
	if (fullscreen) {
		menuBar()->hide();
		statusBar()->hide();
		myToolBar->hide();
		showFullScreen();
		}
	else {
		menuBar()->show();
		statusBar()->show();
		if (toggleToolBarAct->isChecked())
			myToolBar->show();
		showNormal();
	}
}

//zoom in keypress handler
void MainWindow::zoomIn()
{
	float newZoom = view->getZoom() * 2.0;
	view->setZoom(view->getZoom() * 2.0);
	updateZoom(newZoom);
}

//zoom out keypress handler
void MainWindow::zoomOut()
{
	float newZoom = view->getZoom() / 2.0;

	//prevent extreme zooming out
	if (newZoom < 0.01)
		return;

	view->setZoom(newZoom);
	updateZoom(newZoom);
}

//------------------------------------------------------------------------------
// slots receiving signals from the toolbar

void MainWindow::jumpToFirstFrame()
{
	int first = reader.getFirstFrameNum();
	reader.readFrame(first);
}

void MainWindow::jumpToLastFrame()
{
	int last = reader.getLastFrameNum();
	reader.readFrame(last);
}

void MainWindow::jumpToFrameRelative(QAction *a)
{
	int delta = a->data().toInt();
	int first = reader.getFirstFrameNum();
	int last = reader.getLastFrameNum();
	int current = reader.getCurrentFrameNum();
	int dest = current + delta;

	if (dest < first)
		dest = first;

	if (dest > last)
		dest = last;

	reader.readFrame(dest);
}

void MainWindow::jumpToFrameAbsolute()
{
	int first = reader.getFirstFrameNum();
	int last = reader.getLastFrameNum();
	int n = jumpToFrameSpin->value();

	if (n < first)
		n = first;

	if (n > last)
		n = last;

	reader.readFrame(n);
}

//------------------------------------------------------------------------------
// slots receiving signals from the sequence reader

//sequence reader indicates the bit depth of each frame
void MainWindow::processBitDepth(int depth)
{
	//enable the image depth menu when the bit depth of the image is unknown
	//at the moment, this should only be uy16 format
	if (depth == 0)
		imageDepthMenu->setEnabled(true);
	else
		imageDepthMenu->setEnabled(false);

	view->setYUVnativeBitDepth(depth);
}

//sequence reader indicates if each new frame has a header or not
void MainWindow::processHasHeader(bool header)
{
	if (header) {
		//for frames with a header, we can't select picture size or bit depth
		imageFormatMenu->setEnabled(false);
		imageSizeMenu->setEnabled(false);
	}
	else {
		//for frames without a header we must set picture size and bit depth by hand
		imageFormatMenu->setEnabled(true);
		imageSizeMenu->setEnabled(true);
	}
}

//sequence reader indicates the size of each frame
void MainWindow::processSize(QSize &newSize)
{
	QString sizeText = QString("Size %1x%2") .arg(newSize.width()) .arg(newSize.height());
	fileSizeLabel->setText(sizeText);
}

//sequence reader provides new information each time the sequence position changes
void MainWindow::processSequencePosition(const QString &filename,
                                             int frameInFile,
                                             int firstFrame,
                                             int currentFrame,
			                     int lastFrame)
{
	QString positionText = QString("File %1[%2] Frame:%3 [%4-%5]") .arg(filename) .arg(frameInFile) .arg(currentFrame) .arg(firstFrame) .arg(lastFrame);
	fileNameLabel->setText(positionText);

	jumpToFrameSpin->setMinimum(firstFrame);
	jumpToFrameSpin->setMaximum(lastFrame);
	jumpToFrameSpin->setValue(currentFrame);
}

//view generates signals each time the mouse wheel changes the zoom level
void MainWindow::viewImageZoomChanged(float newZoom)
{
	QString zoomText = QString("Zoom %1") .arg(newZoom);
	fileZoomLabel->setText(zoomText);
	updateZoom(newZoom);
}

//------------------------------------------------------------------------------
// Helper functions to keep the menu entries up to date

//select the correct menu entry for this RGB scaling
void MainWindow::updateRgbScaling(bool scaled)
{
	QList<QAction *> actions = rgbScalingGroup->actions();
	for (int i=0; i<actions.count(); i++) {

		if (actions.at(i)->data().toBool() == scaled)
			actions.at(i)->setChecked(true);
	}
}

//select the correct menu entry for this display mode
void MainWindow::updateDisplayMode(View::YUVdisplayMode mode)
{
	QList<QAction *> actions = YUVModeGroup->actions();
	for (int i=0; i<actions.count(); i++) {

		if ((View::YUVdisplayMode)actions.at(i)->data().toInt() == mode)
			actions.at(i)->setChecked(true);
	}
}

//select the correct menu entry for this colour matrix
void MainWindow::updateColourMatrix(ColourSpace::MatrixType matrix)
{
	QList<QAction *> actions = matrixGroup->actions();
	for (int i=0; i<actions.count(); i++) {
		if ((ColourSpace::MatrixType)actions.at(i)->data().toInt() == matrix)
			actions.at(i)->setChecked(true);
	}
}

//select the appropriate image size menu for this size
void MainWindow::updateRawImageSize(SequenceReader::ImageSizes size)
{
	if(size == SequenceReader::SizeCustom) {

		if(imageSizeGroup->checkedAction())
			imageSizeGroup->checkedAction()->setChecked(false);

		imageSizeSetCustomAct->setChecked(true);
		return;
	} else {
		imageSizeSetCustomAct->setChecked(false);
	}

	QList<QAction *> actions = imageSizeGroup->actions();
	for (int i=0; i<actions.count(); i++) {
		if ((SequenceReader::ImageSizes)actions.at(i)->data().toInt() == size) {
			actions.at(i)->setChecked(true);
		} else {
			actions.at(i)->setChecked(false);
		}
	}
}

//select the appropriate bit depth menu for this depth
void MainWindow::updateYUVFormat(int format)
{
	QList<QAction *> actions = imageFormatGroup->actions();
	for (int i=0; i<actions.count(); i++) {
		if (actions.at(i)->data().toInt() == format)
			actions.at(i)->setChecked(true);
	}
}

//select the appropriate bit depth menu for this depth
void MainWindow::updateYUVbitDepth(int depth)
{
	QList<QAction *> actions = imageDepthGroup->actions();
	for (int i=0; i<actions.count(); i++) {
		if (actions.at(i)->data().toInt() == depth)
			actions.at(i)->setChecked(true);
	}
}

//select the appropriate zoom menu entry for this zoom level
void MainWindow::updateZoom(double zoom)
{
	QList<QAction *> actions = zoomGroup->actions();
	for (int i=0; i<actions.count(); i++) {
		if (actions.at(i)->data().toDouble() == zoom)
			actions.at(i)->setChecked(true);
		else
			actions.at(i)->setChecked(false);
	}
}

//select the appropriate scrollbar policy menu entry
void MainWindow::updateScrollBarPolicy(Qt::ScrollBarPolicy policy)
{
	QList<QAction *> actions = scrollBarGroup->actions();
	for (int i=0; i<actions.count(); i++) {
		if ((Qt::ScrollBarPolicy)actions.at(i)->data().toInt() == policy)
			actions.at(i)->setChecked(true);
		else
			actions.at(i)->setChecked(false);
	}
}

//read settings, update the menus to suit and apply to the view object where necessary
void MainWindow::readSettings()
{
	QSettings settings;
	QRect rect = settings.value("geometry", QRect(200, 200, 400, 400)).toRect();
	move(rect.topLeft());
	resize(rect.size());

	recentFiles = settings.value("recentFiles").toStringList();
	updateRecentFileActions();

	bool scaled = settings.value("rgbscaling", true).toBool();
	updateRgbScaling(scaled);
	view->setYUVRGBScaling(scaled);

	View::YUVdisplayMode displayMode =  (View::YUVdisplayMode)settings.value("displaymode", View::YUVdisplayColour).toInt();
	updateDisplayMode(displayMode);
	view->setYUVdisplayMode(displayMode);

	ColourSpace::MatrixType matrixType = (ColourSpace::MatrixType)settings.value("colourmatrix", ColourSpace::HDTVtoRGB).toInt();
	updateColourMatrix(matrixType);
	view->setYUVColourMatrix(matrixType);

	SequenceReader::RawType rawType = (SequenceReader::RawType)settings.value("rawimageformat", SequenceReader::type_auto).toInt();
	updateYUVFormat(rawType);
	reader.setRawFormat(rawType);

	reader.setCustomSize(settings.value("customwidth", 640).toInt(), settings.value("customheight", 480).toInt());
	SequenceReader::ImageSizes imageSize = (SequenceReader::ImageSizes)settings.value("rawimagesize", SequenceReader::SizeHD1080).toInt();
	updateRawImageSize(imageSize);
	reader.setSizeType(imageSize);

	imageSizeSetCustomAct->setText(QString("Custom... [%1x%2]") .arg(reader.getCustomWidth()) .arg(reader.getCustomHeight()));

	Qt::ScrollBarPolicy scrollBarPolicy = (Qt::ScrollBarPolicy)settings.value("scrollbarpolicy", Qt::ScrollBarAlwaysOn).toInt();
	view->setHorizontalScrollBarPolicy(scrollBarPolicy);
	view->setVerticalScrollBarPolicy(scrollBarPolicy);
	updateScrollBarPolicy(scrollBarPolicy);

	int bitDepth = settings.value("rawimagedepth", 10).toInt();
	updateYUVbitDepth(bitDepth);
	view->setYUVbitDepth(bitDepth);

	float zoom = (float)settings.value("zoom", 1.0).toDouble();
	updateZoom(zoom);
	view->setZoom(zoom);
}

void MainWindow::writeSettings(void)
{
	QSettings settings;

	//user selections from the menu
	settings.setValue("colourmatrix", matrixGroup->checkedAction()->data().toInt());

	settings.setValue("customwidth", reader.getCustomWidth());
	settings.setValue("customheight", reader.getCustomHeight());

	if(imageSizeSetCustomAct->isChecked())
		settings.setValue("rawimagesize", SequenceReader::SizeCustom);
	else
		settings.setValue("rawimagesize", imageSizeGroup->checkedAction()->data().toInt());

	settings.setValue("rawimagedepth", imageDepthGroup->checkedAction()->data().toInt());
	settings.setValue("rawimageformat", imageFormatGroup->checkedAction()->data().toInt());
	settings.setValue("rgbscaling", rgbScalingGroup->checkedAction()->data().toBool());
	settings.setValue("displaymode", YUVModeGroup->checkedAction()->data().toInt());
	settings.setValue("zoom", zoomGroup->checkedAction()->data().toDouble());
	settings.setValue("scrollbarpolicy", scrollBarGroup->checkedAction()->data().toInt());

	//information about the window
	settings.setValue("geometry", geometry());
	settings.setValue("recentfiles", recentFiles);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	writeSettings();
	event->accept();
}

void MainWindow::helpViewerClosed()
{
  assistantClientStarted = false;
}
