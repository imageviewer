/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <assert.h>
#include <math.h>
#include <stdint.h>

#include "rawIOleader-frm.h"

/* file format documentation from:
 *   http://www.leaderusa.com/web/dl_count.php?ftID=1&fDR=products%2Fmanual%2F01_Waveform_Monitor%2F12_LV5800%2F&fNM=LV5800+LV58SER01-01A+SDI+Input+Instruction+Manual.pdf
 */

void RawReaderLeaderFRM::readHeader()
{
	/* file format is little endian */
	/* the first two bytes (addresses 0-1) are reserved */
	getc(infile);
	getc(infile);
	/* the next four bytes (addresses 2 to 5) indicate the size of the captured data.
	 * In a dual link, half of the size of the captured data, which is equivalent to
	 * the size of one link will be indicated */
	uint32_t size = 0;
	size |= getc(infile);
	size |= getc(infile) << 8;
	size |= getc(infile) << 16;
	size |= getc(infile) << 24;
	/* next two bytes indicate video format */
	uint16_t vfmt = 0;
	vfmt |= getc(infile);
	vfmt |= getc(infile) << 8;
	/* the 56 bytes contained in addresses 8 through 0x3f are reserved */
	for (int i = 0; i < 56; i++) getc(infile);
	_sd = 0;
	switch (vfmt) {
	case 0x0000: /* 30 fps, interlaced/psf */
	case 0x0001: /* 29.97 fps, interlaced/psf */
	case 0x0002: /* 25 fps, interlaced/psf */
	case 0x0004: /* 24 fps, psf */
	case 0x0005: /* 23.98 fps, psf */
	case 0x000a: /* 30p */
	case 0x000b: /* 29.97p */
	case 0x000c: /* 25p */
	case 0x000e: /* 24p */
	case 0x000f: /* 23.98p */
		_width = 1920;
		_height = 1080;
		_depth = 10;
		/* xxx, actually need to give the full raster width */
		/* 10bit 4:2:2 */
		break;
	case 0x0106: /* 60p */
	case 0x0107: /* 59.94p */
	case 0x0108: /* 50p */
	case 0x010a: /* 30p */
	case 0x010b: /* 29.97p */
	case 0x010c: /* 25p */
	case 0x010e: /* 24p */
	case 0x010f: /* 23.98p */
		_width = 1280;
		_height = 720;
		_depth = 10;
		/* xxx, actually need to give the full raster width */
		/* 10bit 4:2:2 */
		break;
	case 0x0201: _sd=1; _depth = 10; _width = 858; _height = 525; /* 29.97i */ break;
	case 0x0202: _sd=1; _depth = 10; _width = 864; _height = 625; /* 25i */ break;
	default:
		assert(!"unhandled mode");
	}
	/* todo: support the other chroma modes */
	_chroma = RawFrame::Cr422;
}

RawFrame& RawReaderLeaderFRM::read(RawFrame &f)
{
	assert(f.chroma == _chroma);

	/* 10bits of digital data are expressed wih one word (16bits) */
	/* order of words for SD: Cb0, xxx, Y0, xxx, Cr0, xxx, Y1, xxx... */
	if (_sd) {
		for (int y = 0; y < _height; y++) {
			for (int x = 0; x < _width; x+=2) {
				f.cb[y][x/2] = getc(infile);
				f.cb[y][x/2] |= (getc(infile) & 0x3) << 8;
				getc(infile); getc(infile);
				f.luma[y][x] = getc(infile);
				f.luma[y][x] |= (getc(infile) & 0x3) << 8;
				getc(infile); getc(infile);
				f.cr[y][x/2] = getc(infile);
				f.cr[y][x/2] |= (getc(infile) & 0x3) << 8;
				getc(infile); getc(infile);
				f.luma[y][x+1] = getc(infile);
				f.luma[y][x+1] |= (getc(infile) & 0x3) << 8;
				getc(infile); getc(infile);
			}
		}
	} else {
		for (int y = 0; y < _height; y++) {
			for (int x = 0; x < _width; x+=2) {
				f.luma[y][x] = getc(infile);
				f.luma[y][x] |= (getc(infile) & 0x3) << 8;
				f.cb[y][x/2] = getc(infile);
				f.cb[y][x/2] |= (getc(infile) & 0x3) << 8;
				f.luma[y][x+1] = getc(infile);
				f.luma[y][x+1] |= (getc(infile) & 0x3) << 8;
				f.cr[y][x/2] = getc(infile);
				f.cr[y][x/2] |= (getc(infile) & 0x3) << 8;
			}
		}
	}

	return f;
}

RawFrame& RawReaderLeaderFRM::read()
{
	throw;
}

