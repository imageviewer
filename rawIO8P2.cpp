/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <assert.h>
#include "rawIO8P2.h"

RawFrame& RawReader8P2::read(RawFrame& f)
{
	assert(f.chroma == RawFrame::Cr422);
	const int line_len = f.luma.width();
	unsigned char* in_line = new unsigned char[line_len];

	/* read luma plane */
	for (int y = 0; y < f.luma.height(); y++) {
		if (fread(in_line, line_len, 1, file) < 1)
			break;
		for (int x = 0; x < f.luma.width(); x++)
			f.luma[y][x] = in_line[x];
	}

	/* read c1 plane */
	const int chroma_line_len = line_len/2;
	for (int y = 0; y < f.cb.height(); y++) {
		if (fread(in_line, chroma_line_len, 1, file) < 1)
			break;
		for (int x = 0; x < f.cb.width(); x++)
			f.cb[y][x] = in_line[x];
	}

	/* read c2 plane */
	for (int y = 0; y < f.cr.height(); y++) {
		if (fread(in_line, chroma_line_len, 1, file) < 1)
			break;
		for (int x = 0; x < f.cr.width(); x++)
			f.cr[y][x] = in_line[x];
	}

	free(in_line);
	return f;
}

RawFrame& RawReader8P2::read()
{
	throw;
}

void RawWriter8P2::write(const RawFrame& f) const
{
	assert(f.chroma == RawFrame::Cr422);
	const int line_len = f.luma.width();
	unsigned char* out_line = new unsigned char[line_len];

	/* write luma plane */
	for (int y = 0; y < f.luma.height(); y++) {
		for (int x = 0; x < f.luma.width(); x++)
			out_line[x] = f.luma[y][x];

		fwrite(out_line, line_len, 1, file);
	}

	/* write c1 plane */
	const int chroma_line_len = line_len/2;
	for (int y = 0; y < f.cb.height(); y++) {
		for (int x = 0; x < f.cb.width(); x++)
			out_line[x] = f.cb[y][x];

		fwrite(out_line, chroma_line_len, 1, file);
	}

	/* write c2 plane */
	for (int y = 0; y < f.cr.height(); y++) {
		for (int x = 0; x < f.cr.width(); x++)
			out_line[x] = f.cr[y][x];

		fwrite(out_line, chroma_line_len, 1, file);
	}

	free(out_line);
}
