HEADERS += rawIOrgba.h \
    rawIOrgb.h \
    mainwindow.h \
    view.h \
    transformDialog.h \
    customSizeDialog.h \
    chromaResample.h \
    colourSpace.h \
    sequenceReader.h
HEADERS += array_df.h \
    rawFrame.h \
    rawFrameIO.h
HEADERS += rawIOv210.h \
    rawIOv216.h \
    rawIOYV12.h \
    rawIOuyvy.h \
    rawIOuy16.h \
    rawIOi420.h \
    rawIOyy16.h \
    rawIOyyy8.h
HEADERS += rawIOppm.h \
    rawIOpgm.h \
    rawIOsgi.h \
    rawIOleader-frm.h
HEADERS += rawIO16P0.h \
    rawIO16P2.h \
    rawIO16P4.h \
    rawIO8P2.h
SOURCES += rawIOrgba.cpp \
    rawIOrgb.cpp \
    main.cpp \
    sequenceReader.cpp
SOURCES += mainwindow.cpp \
    view.cpp \
    transformDialog.cpp \
    customSizeDialog.cpp \
    chromaResample.cpp \
    colourSpace.cpp
SOURCES += rawIOv210.cpp \
    rawIOv216.cpp \
    rawIOYV12.cpp \
    rawIOuyvy.cpp \
    rawIOuy16.cpp \
    rawIOi420.cpp \
    rawIOyy16.cpp \
    rawIOyyy8.cpp
SOURCES += rawIOppm.cpp \
    rawIOpgm.cpp \
    rawIOsgi.cpp \
    rawIOleader-frm.cpp
SOURCES += rawIO16P0.cpp \
    rawIO16P2.cpp \
    rawIO16P4.cpp \
    rawIO8P2.cpp
TEMPLATE = app
TARGET = imageviewer
RESOURCES = imageviewer.qrc
HELP_SOURCES = documentation/imageviewer.qhcp
CONFIG += embed_mainfest_exe \
    assistant \
    debug_and_release
QT += svg

win32 { 
    # icon resource on windows
    RC_FILE = imageviewer.rc
    executable.path = win-install
    executable.files = release/imageviewer.exe
    MY_QT_VER = $$[QT_VERSION]
    QT_VER = $$find(MY_QT_VER, 4.[4-9].[0-9]
    isEmpty(QT_VER):executable.files += $$(QTDIR)/bin/assistant.exe
    else:# after Qt 4.4.0, the help system changed so we need the legacy one
    executable.files += $$(QTDIR)/bin/assistant_adp.exe
    documentation.path = win-install/documentation
    documentation.files = documentation/imageviewer.qhc
    install_libraries.path = win-install
    install_libraries.files = $$(QTDIR)/bin/QtCore4.dll \
        $$(QTDIR)/bin/QtGui4.dll
    install_libraries.files += $$(QTDIR)/bin/QtSvg4.dll
    
    # for assistant[_adp].exe
    install_libraries.files += $$(QTDIR)/bin/QtXml4.dll
    install_libraries.files += $$(QTDIR)/bin/QtNetwork4.dll
    icon_plugin.path = win-install/iconengines
    icon_plugin.files = $$(QTDIR)/plugins/iconengines/qsvgicon4.dll
    INSTALLS += executable
    INSTALLS += documentation
    INSTALLS += install_libraries
    INSTALLS += icon_plugin
}
unix { 
    # destination directory for the executable
    executable.path = /usr/local/bin/
    executable.files = imageviewer
    
    # destination directory for the docs
    documentation.path = /usr/share/imageviewer/
    documentation.files = documentation/imageviewer.qhc
    DEFINES += DOCDIR=\\\"$$documentation.path\\\"
    INSTALLS += executable
    INSTALLS += documentation
}

# attempt to integrate building of the qhc (helpcollection) file with the build
# - still problematic - if helpgen.CONFIG = no_link is set the helpcollection target never gets built.
# - if this is not set the qhc file gets built but is also added to the OBJECTS list which is passed to the linker,
# which subsequently fails.  

# QMAKE_EXTRA_TARGETS += helpcollection dummy
# QMAKE_EXTRA_COMPILERS += helpgen dummygen

# dummy.target = dummy.o
# dummy.depends = helpcollection
# dummygen.output = dummy.o
# dummygen.commands = echo \
#     \"void \
#     dummy(void){};\" \
#     > \
#     dummy.c; \
#     g++ \
#     -c \
#     -o \
#     dummy.o \
#     dummy.c
# helpcollection.target = documentation/imageviewer.qhc
# helpcollection.depends = documentation/imageviewer.qhcp
# helpgen.output = documentation/imageviewer.qhc
# helpgen.input = HELP_SOURCES
# helpgen.commands = qcollectiongenerator \
#     ${QMAKE_FILE_NAME}
# helpgen.CONFIG = no_link
