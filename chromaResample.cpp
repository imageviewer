/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include "chromaResample.h"
#include "rawFrame.h"

const RawFrame& ChromaResample::process(const RawFrame& src)
{
	process_ext(src, _resampled);
	return _resampled;
}

//change the source chroma sampling
void ChromaResample::process_ext(const RawFrame &src, RawFrame &dest)
{
	switch (dest.chroma) {

	case RawFrame::CrRGB:
		//doh!
		assert(false);
		throw;

	case RawFrame::Cr444:
		to444(src, dest);
		break;

	case RawFrame::Cr422:
		to422(src, dest);
		break;

	case RawFrame::Cr420:
		to420(src, dest);
		break;

	case RawFrame::Cr411:
		to411(src, dest);
		break;

	case RawFrame::YOnly:
		toYonly(src, dest);
		break;
	}
}

void ChromaResample::copyArray(const Array2d<int>& src, Array2d<int>& dest)
{
	assert(src.width() == dest.width());
	assert(src.height() == dest.height());

	dest = src;
}

void ChromaResample::to422(const RawFrame& src, RawFrame& dest)
{
	copyArray(src.luma, dest.luma);

	switch (src.chroma) {

	case RawFrame::Cr422:
		//just copy it
		copyArray(src.cb, dest.cb);
		copyArray(src.cr, dest.cr);
		break;

	case RawFrame::Cr444:
		//horizontally subsample
		horizSubsample(src.cb, dest.cb);
		horizSubsample(src.cr, dest.cr);
		break;

	case RawFrame::Cr420:
		//vertically interpolate
		vertInterp(src.cb, dest.cb);
		vertInterp(src.cr, dest.cr);
		break;

	case RawFrame::Cr411:
		//horizontally interpolate
		horizInterp(src.cb, dest.cb);
		horizInterp(src.cr, dest.cr);
		break;

	case RawFrame::YOnly:
		dest.cb.assign(0);
		dest.cr.assign(0);

	case RawFrame::CrRGB:
		throw;

		break;
	}
}

void ChromaResample::to420(const RawFrame& src, RawFrame& dest)
{
	copyArray(src.luma, dest.luma);

	switch (src.chroma) {

	case RawFrame::Cr422:
		//vertically subsample
		vertSubsample(src.cb, dest.cb);
		vertSubsample(src.cr, dest.cr);

		break;

	case RawFrame::Cr444:
	{
		Array2d<int> hsub(src.luma.width()/2, src.luma.height());

		//horizontally then vertically subsample
		horizSubsample(src.cb, hsub);
		vertSubsample(hsub, dest.cb);

		horizSubsample(src.cr, hsub);
		vertSubsample(hsub, dest.cr);
	}
		break;

	case RawFrame::Cr420:
		//just copy it
		copyArray(src.cb, dest.cb);
		copyArray(src.cr, dest.cr);
		break;

	case RawFrame::Cr411:
	{
		Array2d<int> hinterp(src.luma.width()*2, src.luma.height());

		//horizontally interpolate then vertically subsample
		horizInterp(src.cb, hinterp);
		vertSubsample(hinterp, dest.cb);

		horizInterp(src.cr, hinterp);
		vertSubsample(hinterp, dest.cr);
	}
		break;

	case RawFrame::YOnly:
		dest.cb.assign(0);
		dest.cr.assign(0);
		break;

	case RawFrame::CrRGB:
		throw;
	}
}

void ChromaResample::to444(const RawFrame& src, RawFrame& dest)
{
	copyArray(src.luma, dest.luma);

	switch (src.chroma) {

	case RawFrame::Cr422:
		//interpolate horizontally
		horizInterp(src.cb, dest.cb);
		horizInterp(src.cr, dest.cr);
		break;

	case RawFrame::Cr444:
		//just copy it
		copyArray(src.cb, dest.cb);
		copyArray(src.cr, dest.cr);
		break;

	case RawFrame::Cr420:
	{
		Array2d<int> vinterp(src.luma.width()/2, src.luma.height());

		//vertically then horizontally interpolate
		vertInterp(src.cb, vinterp);
		horizInterp(vinterp, dest.cb);

		vertInterp(src.cr, vinterp);
		horizInterp(vinterp, dest.cr);
	}
		break;

	case RawFrame::Cr411:
	{
		Array2d<int> hinterp(src.luma.width()/2, src.luma.height());

		//horizontally interpolate twice
		horizInterp(src.cb, hinterp);
		horizInterp(hinterp, dest.cb);

		horizInterp(src.cr, hinterp);
		horizInterp(hinterp, dest.cr);
	}
		break;

	case RawFrame::YOnly:
		dest.cb.assign(0);
		dest.cr.assign(0);
		break;

	case RawFrame::CrRGB:
		throw;
	}
}

void ChromaResample::to411(const RawFrame& src, RawFrame& dest)
{
	copyArray(src.luma, dest.luma);

	switch (src.chroma) {

	case RawFrame::Cr422:
		//subsample horizontally
		horizSubsample(src.cb, dest.cb);
		horizSubsample(src.cr, dest.cr);
		break;

	case RawFrame::Cr444:
	{
		Array2d<int> hsub(src.luma.width()/2, src.luma.height());

		//subsample horizontally twice
		horizSubsample(src.cb, hsub);
		horizSubsample(hsub, dest.cb);

		horizSubsample(src.cr, hsub);
		horizSubsample(hsub, dest.cr);
	}
		break;

	case RawFrame::Cr420:
	{
		Array2d<int> vinterp(src.luma.width()*2, src.luma.height());

		//interpolate vertically then subsample horizontally
		vertInterp(src.cb, vinterp);
		horizSubsample(vinterp, dest.cb);

		vertInterp(src.cr, vinterp);
		horizSubsample(vinterp, dest.cr);
	}
		break;

	case RawFrame::Cr411:
		//just copy it
		copyArray(src.cb, dest.cb);
		copyArray(src.cr, dest.cr);
		break;

	case RawFrame::YOnly:
		dest.cb.assign(0);
		dest.cr.assign(0);
		break;

	case RawFrame::CrRGB:
		throw;
	}
}

void ChromaResample::toYonly(const RawFrame &src, RawFrame& dest)
{
	//input can't be RGB
	assert(src.chroma != RawFrame::CrRGB);

	//no conversion required
	copyArray(src.luma, dest.luma);
}

//filtering functions
void ChromaResample::horizSubsample(const Array2d<int>& in, Array2d<int>& out)
{
	//FIXME - this needs rewriting like the interpolator code and testing

	assert(in.width() / 2 == out.width());
	assert(in.height() == out.height());

	Array1d<int> buffer(in.width()+2);

	for (int line=0; line<in.height(); line++) {

		for (int pixel = 0; pixel<in.width(); pixel+=2) {

			int C=0;

			if (pixel>0)
				C += in[line][pixel-1];
			else
				C += in[line][pixel];

			C+= in[line][pixel] * 2;

			if (pixel < in.width()-1)
				C+= in[line][pixel+1];
			else
				C+= in[line][pixel];

			out[line][pixel/2] = C;
		}
	}
}

void ChromaResample::vertSubsample(const Array2d<int>& in, Array2d<int> &out)
{
	//FIXME - this needs rewriting like the interpolator code and testing

	assert(in.width() == out.width());
	assert(in.height() / 2 == out.height());

	for (int line=0; line<in.height(); line+=2) {
		for (int pixel = 0; pixel<in.width(); pixel++) {

			int C=0;

			if (line>0)
				C += in[line-1][pixel];
			else
				C += in[line][pixel];

			C+= in[line][pixel] * 2;

			if (line < in.width()-1)
				C+= in[line+1][pixel];
			else
				C+= in[line][pixel];

			out[line/2][pixel] = C;
		}
	}

}

void ChromaResample::horizInterp(const Array2d<int>& in, Array2d<int>& out)
{
	assert(in.width() == out.width() / 2);
	assert(in.height() == out.height());

	Array1d<int> buffer(out.width()+2);
	buffer.assign(0);

	/* filter */
	for (int line=0; line<in.height(); line++) {

		//copy input data
		for (int pixel=0, x=1; pixel<in.width(); pixel++, x+=2)
			buffer[x] = in[line][pixel];

		//edge extend to get the last pixel correct
		buffer[out.width() + 1] = in[line][in.width() - 1];

		//filter
		for (int pixel=0, x=1; pixel<out.width(); pixel++, x++) {
			out[line][pixel] = (buffer[x-1] + 2*buffer[x] + buffer[x+1] + 1)
			    >> 1;
		}
	}
}

void ChromaResample::vertInterp(const Array2d<int>& in, Array2d<int> &out)
{
	assert(in.width() == out.width());
	assert(in.height() == out.height() / 2);

	Array1d<int> buffer(out.height()+2);
	buffer.assign(0);

	/* filter */
	for (int column=0; column<in.width(); column++) {

		//copy input data
		for (int pixel=0, x=1; x<out.height(); pixel++, x+=2)
			buffer[x] = in[pixel][column];

		//edge extend to get the last pixel correct
		buffer[out.height() + 1] = in[in.height() - 1][column];

		//filter
		for (int pixel=0, x=1; pixel<out.height(); pixel++, x++)
			out[pixel][column] = (buffer[x-1] + 2*buffer[x] + buffer[x+1] + 1)
			    >> 1;
	}
}
