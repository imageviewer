/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef VIEW_H
#define VIEW_H

#include "rawFrame.h"
#include "colourSpace.h"
#include "sequenceReader.h"

#include <QGraphicsView>

class QGraphicsScene;

class View : public QGraphicsView
{
	Q_OBJECT
public:
	View(QWidget *parent = 0);

	enum ProcessType {processResample=0, processColour, processDisplay, processScene};


	enum YUVdisplayMode {
		YUVdisplayColour,
		YUVdisplayYOnly,
		YUVdisplayPlanar
	};

	float getZoom(void);
	int getRotation(void);

signals:
	void pixelInfoMessage(const QString &, int);
	void zoomChanged(float);

public slots:
	void setZoom(float);
	void setYUVRGBScaling(bool scaled);
	void setYUVColourMatrix(ColourSpace::MatrixType);
	void setYUVdisplayMode(YUVdisplayMode);
	void setYUVbitDepth(int);                    //the bit depth from the gui menu
	void setYUVnativeBitDepth(int);              //the bit depth of the original file, or 0 if unknown

private slots:
	void setupMatrix();
	void showFrame(const RawFrame*);

private:
	void invalidateFrame();
	void processImage(const ProcessType type);

	//deal with mouse interaction
	void wheelEvent(QWheelEvent* wheelEvent);
	void mouseDoubleClickEvent(QMouseEvent * e);

	float zoom;
	int rotation;

	//QGraphicsView *graphicsView;
	QGraphicsScene *scene;

	//information for displaying a YUV image from a headerless file
	int yuvImageBitDepth;
        int yuvImageNativeBitDepth;
	bool yuvImageRGBScaling;
	ColourSpace::MatrixType yuvImageColourMatrix;
	RawFrame::Chroma inChroma;
	SequenceReader::RawType yuvFormat;

	//stores for loading, resampling and colour converting the input
	RawFrame *inFrame;
	RawFrame *frame444;
	RawFrame *frameRGB;

	//settings that change the appearance of the image
	YUVdisplayMode yuvDisplayMode;
};

#endif
