/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <assert.h>
#include "rawIOv210.h"

typedef unsigned int uint_t;
typedef unsigned char uint8_t;

//read the first 10 bit sample from the least significant bits of the 4 bytes pointed to by 'data'
inline uint_t readv210sample_pos2of3(uint8_t* data)
{
	const uint_t lsb = (data[2] & 0xf0) >> 4;
	const uint_t msb = (data[3] & 0x3f) << 4;
	return msb | lsb;
}

//read the second 10 bit sample from the middle bits of the 4 bytes pointed to by 'data'
inline uint_t readv210sample_pos1of3(uint8_t* data)
{
	const uint_t lsb = (data[1] & 0xfc) >> 2;
	const uint_t msb = (data[2] & 0x0f) << 6;
	return msb | lsb;
}

//read the third 10 bit sample from the more significant bits of the 4 bytes pointed to by 'data'
inline uint_t readv210sample_pos0of3(uint8_t* data)
{
	const uint_t lsb = (data[0] & 0xff);
	const uint_t msb = (data[1] & 0x03) << 8;
	return msb | lsb;
}

void unpackv210line(uint8_t *data, const int y, RawFrame &dst)
{
	uint8_t* ptr;
	int x;

	/* unpack luma */
	const int num_firstpass_samples_luma = (dst.luma.width()/3)*3; //actual (not padded) number of sets of 3 luma samples in line
	ptr = data;
	for (x = 0; x < num_firstpass_samples_luma; ptr += 8) {
		dst.luma[y][x++] = readv210sample_pos1of3(ptr + 0);
		dst.luma[y][x++] = readv210sample_pos0of3(ptr + 4);
		dst.luma[y][x++] = readv210sample_pos2of3(ptr + 4);
	}

	if (x<dst.luma.width()) {
		dst.luma[y][x] = readv210sample_pos1of3(ptr + 0);
		x++;
	}

	if (x<dst.luma.width()) {
		dst.luma[y][x] = readv210sample_pos0of3(ptr + 4);
	}

	/* unpack choma */
	const int num_firstpass_samples_chroma = (dst.luma.width()/2/3)*3;
	ptr = data;
	for (x = 0; x < num_firstpass_samples_chroma; ptr += 16) {
		dst.cb[y][x] = readv210sample_pos0of3(ptr + 0);
		dst.cr[y][x] = readv210sample_pos2of3(ptr + 0);
		x++;

		dst.cb[y][x] = readv210sample_pos1of3(ptr + 4);
		dst.cr[y][x] = readv210sample_pos0of3(ptr + 8);
		x++;

		dst.cb[y][x] = readv210sample_pos2of3(ptr + 8);
		dst.cr[y][x] = readv210sample_pos1of3(ptr + 12);
		x++;
	}

	if (x<dst.luma.width()/2) {
		dst.cb[y][x] = readv210sample_pos0of3(ptr + 0);
		dst.cr[y][x] = readv210sample_pos2of3(ptr + 0);
		x++;
	}

	if (x<dst.luma.width()/2) {
		dst.cb[y][x] = readv210sample_pos1of3(ptr + 4);
		dst.cr[y][x] = readv210sample_pos0of3(ptr + 8);
	}

}

RawFrame& RawReaderv210::read(RawFrame &f)
{
	//there must be an even number of luma samples because v210 is 4:2:2
	assert((f.luma.width() & 1) == 0);
	assert(f.chroma == RawFrame::Cr422);

	/* pad with to a multiple of 48 luma samples */
	/* nb, 48 luma samples becomes 128 bytes */
	const uint_t padded_w = ((f.luma.width() + 47)/48) * 48; //number of luma samples, padded
	const uint_t padded_line_length = (2*padded_w*4) / 3; //number of bytes on each line in the file, includin padding data

	uint8_t *data = new uint8_t[padded_line_length];
	size_t linesRead;

	for (int y=0; y<f.luma.height(); y++) {

		linesRead = fread(data, padded_line_length, 1, infile);
		if (linesRead < 1)
			break;
		unpackv210line(data, y, f);
	}

	//if (feof (infile))
	//   throw /* some eof/shortread exception */ ;

	return f;
}

RawFrame& RawReaderv210::read()
{
	throw /* unknown framesize */;
}

void RawWriterv210::write(const RawFrame& f) const
{
	//default to 10 bits
	write(f, 10);
}

void RawWriterv210::write(const RawFrame& f, const int depth) const
{
	const int Yoffset = 0; //Y has no offset
	const int shift = 10 - depth; //no shift for 10 bits, 2 bits shift for 8 bit source
	const int UVoffset = 0;

	int v210LineLength = (2 * f.luma.width() * 4) / 3; //there are three components in 4 bytes
	char *outLine = (char *)calloc(v210LineLength, sizeof(char));
	assert(f.chroma == RawFrame::Cr422);

	for (int y = 0; y < f.luma.height(); y++) {

		/* pack to v210 format */
		/* do the luminance first */
		int i=0;
		for (int x=0; x<v210LineLength; x+=16) {
			int pix=f.luma[y][i++];
			pix+=Yoffset;
			pix <<= shift;

			outLine[x+1] = (pix << 2) & 0xFC;
			outLine[x+2] = (pix >> 6) & 0x0F;

			pix=f.luma[y][i++];
			pix+=Yoffset;
			pix <<= shift;

			outLine[x+4] = pix & 0xFF;
			outLine[x+5] = (pix >> 8) & 0x03;

			pix=f.luma[y][i++];
			pix+=Yoffset;
			pix <<= shift;

			outLine[x+6] = (pix << 4) & 0xF0;
			outLine[x+7] = (pix >> 4) & 0x3F;

			pix=f.luma[y][i++];
			pix+=Yoffset;
			pix <<= shift;

			outLine[x+9] = (pix << 2) & 0xFC;
			outLine[x+10] = (pix >> 6) & 0x0F;

			pix=f.luma[y][i++];
			pix+=Yoffset;
			pix <<= shift;

			outLine[x+12] = pix & 0xFF;
			outLine[x+13] = (pix >> 8) & 0x03;

			pix=f.luma[y][i++];
			pix+=Yoffset;
			pix <<= shift;

			outLine[x+14] = (pix << 4) & 0xF0;
			outLine[x+15] = (pix >> 4) & 0x3F;
		}

		//do the chroma
		i=0;
		for (int x=0; x<v210LineLength; x+=16) {
			//Cb0
			int pix=f.cb[y][i];
			pix+=UVoffset;
			pix <<= shift;

			outLine[x] = pix & 0xFF;
			outLine[x+1] |= (pix >> 8) & 0x03;

			//Cr0
			pix=f.cr[y][i++];
			pix+=UVoffset;
			pix <<= shift;

			outLine[x+2] |= (pix << 4) & 0xF0;
			outLine[x+3] = (pix >> 4) & 0x3F;

			//Cb1
			pix=f.cb[y][i];
			pix+=UVoffset;
			pix <<= shift;

			outLine[x+5] |= (pix << 2) & 0xFC;
			outLine[x+6] |= (pix >> 6) & 0x0F;

			//Cr1
			pix=f.cr[y][i++];
			pix+=UVoffset;
			pix <<= shift;

			outLine[x+8] = pix & 0xFF;
			outLine[x+9] |= (pix >> 8) & 0x03;

			//Cb2
			pix=f.cb[y][i];
			pix+=UVoffset;
			pix <<= shift;

			outLine[x+10] |= (pix << 4) & 0xF0;
			outLine[x+11] = (pix >> 4) & 0x3F;

			//Cr2
			pix=f.cr[y][i++];
			pix+=UVoffset;
			pix <<= shift;

			outLine[x+13] |= (pix << 2) & 0xFC;
			outLine[x+14] |= (pix >> 6) & 0x0F;
		}

		/* write line out */
		fwrite(outLine, v210LineLength, 1, outfile);
	}

	free(outLine);
}

