/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>

#include "view.h"
#include "colourSpace.h"

class SequenceReader;

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	MainWindow();

protected:

private slots:
	void setZoom(QAction *);
	void setYUVdisplayMode(QAction *);
	void setRGBScaling(QAction *);
	void setColourMatrix(QAction *);
	void setCustomSize(bool);
	void setPresetSize(QAction *);
	void setYUVimageDepth(QAction *);
	void setYUVimageFormat(QAction *);
	void setScrollBar(QAction *);
	void setFullScreen(bool);
	void toggleFullScreen();
	void escapeFullScreen();
	void zoomIn();
	void zoomOut();
	void processSize(QSize &);
	void viewImageZoomChanged(float);
	void processSequencePosition(const QString&, int, int, int, int);
	void processHasHeader(bool);
	void processBitDepth(int);
	void jumpToFirstFrame();
	void jumpToFrameRelative(QAction *);
	void jumpToFrameAbsolute();
	void jumpToLastFrame();
	void openDirectory();
	void openSeq();
	void openRecentFile();
	void closeEvent(QCloseEvent *event);
	void about();
	void assistant();
	void toggleToolBar();
	void helpViewerClosed();

private:
	void readSettings();
	void writeSettings();
	void updateRecentFileActions();
	void updateRgbScaling(bool scaled);
	void updateDisplayMode(View::YUVdisplayMode mode);
	void updateColourMatrix(ColourSpace::MatrixType matrix);
	void updateRawImageSize(SequenceReader::ImageSizes size);
	void updateYUVbitDepth(int depth);
	void updateYUVFormat(int format);
	void updateZoom(double zoom);
	void updateScrollBarPolicy(Qt::ScrollBarPolicy policy);

	void createActions();
	void createStatusBar();
	void createToolBar();
	void initializeAssistant();

	QProcess *assistantClientProcess;
	bool  assistantClientStarted;

	//file menu actions
	QStringList recentFiles;
	QAction *recentFileSeperatorAct;
	enum {MaxRecentFiles = 5};
	QAction *recentFileAct[MaxRecentFiles];

	//view menu actions and groups
	QActionGroup *YUVModeGroup; //YUV display mode

	QActionGroup *zoomGroup;

	QActionGroup *rgbScalingGroup; //YUV->RGB scaling is 1:1 or 255/219
	QActionGroup *matrixGroup;     //Use either the HDTV or SDTV colour matrix
	QActionGroup *scrollBarGroup; 

	QAction *toggleToolBarAct;

	//image menu actions and groups
	QMenu *imageFormatMenu;
	QActionGroup *imageFormatGroup;

	QMenu *imageSizeMenu;
	QMenu *imageDepthMenu;
	QActionGroup *imageSizeGroup;
	QAction *imageSizeSetCustomAct;

	QActionGroup *imageDepthGroup;

	//GUI items
	View *view;
	SequenceReader reader;

	//status bar
	QStatusBar *myStatusBar;
	QLabel *fileFormatLabel;
	QLabel *fileSizeLabel;
	QLabel *fileZoomLabel;
	QLabel *fileNameLabel;

	//Tool bar
	QToolBar *myToolBar;
	QSpinBox *jumpToFrameSpin;

};

#endif
