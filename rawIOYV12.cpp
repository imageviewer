/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <assert.h>
#include "rawIOYV12.h"

// yv12 is 4:2:0 planar Y, Cr, Cb

RawFrame& RawReaderYV12::read(RawFrame& f)
{
	assert(f.chroma == RawFrame::Cr420);

	size_t linesRead;
	unsigned char *inLine = new unsigned char[f.luma.width()];

	//luminance
	for (int y = 0; y < f.luma.height(); y++) {

		linesRead = fread(inLine, f.luma.width(), 1, infile);

		for (int x = 0; x < f.luma.width(); x++)
			f.luma[y][x] = inLine[x];

		if (linesRead < 1)
			goto exit;
	}

	//Cr
	for (int y = 0; y < f.cr.height(); y++) {

		linesRead = fread(inLine, f.cr.width(), 1, infile);

		for (int x = 0; x < f.cr.width(); x++)
			f.cr[y][x] = inLine[x];

		if (linesRead < 1)
			goto exit;
	}

	//Cb
	for (int y = 0; y < f.cb.height(); y++) {

		linesRead = fread(inLine, f.cb.width(), 1, infile);

		for (int x = 0; x < f.cb.width(); x++)
			f.cb[y][x] = inLine[x];

		if (linesRead < 1)
			goto exit;
	}

	exit:

	free(inLine);
	return f;
}

RawFrame& RawReaderYV12::read()
{
	throw /* unknown size */;
}

void RawWriterYV12::write(const RawFrame& f) const
{
	assert(f.chroma == RawFrame::Cr420);
	for (int y = 0; y < f.luma.height(); y++)
		for (int x = 0; x < f.luma.width(); x++)
			fputc(f.luma[y][x], outfile);

	for (int y = 0; y < f.cr.height(); y++)
		for (int x = 0; x < f.cr.width(); x++)
			fputc(f.cr[y][x], outfile);

	for (int y = 0; y < f.cb.height(); y++)
		for (int x = 0; x < f.cb.width(); x++)
			fputc(f.cb[y][x], outfile);
}
