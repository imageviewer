/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __RAWIOPGM_H
#define __RAWIOPGM_H

#include <stdio.h>
#include "rawFrameIO.h"

class RawWriterpgm : public RawWriter {
public:
	RawWriterpgm(FILE *fd, int d) :
		outfile(fd), depth(d)
	{
		/* pgm may only have a maxval of 65535 */
		assert(d <= 16);
	};

	void write(const RawFrame& f) const
	{
		switch (f.chroma) {
		case RawFrame::Cr422: /* fall through */
		case RawFrame::Cr420:
			writeCr42x(f);
			break;
		case RawFrame::YOnly:
			writeYOnly(f);
			break;
		default:
			throw /* invalid frame type */;
		}
	}

private:
	void writeCr42x(const RawFrame&) const;
	void writeYOnly(const RawFrame&) const;

	FILE *outfile;
	int depth;
};

class RawReaderpgm : public RawReader {
public:
	RawReaderpgm(FILE *fd) :
		infile(fd)
	{
		readHeader();
	}
	;

	RawFrame& read(RawFrame&);
	RawFrame& read();

private:
	void readHeader();

	int _maxval;
	FILE *infile;
};

#endif
