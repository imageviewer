/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <assert.h>
#include "rawIO16P0.h"

RawFrame& RawReader16P0::read(RawFrame& f)
{
	assert(f.chroma == RawFrame::Cr420);
	const int line_len = f.luma.width() * 2;
	unsigned char* in_line = new unsigned char[line_len];
	unsigned char* ptr;

	/* read luma plane */
	for (int y = 0; y < f.luma.height(); y++) {
		if (fread(in_line, line_len, 1, file) < 1)
			break;
		ptr = in_line;
		for (int x = 0; x < f.luma.width(); x++, ptr+=2)
			f.luma[y][x] = ((ptr[0] << 8) | ptr[1]);
	}

	/* read c1 plane */
	const int chroma_line_len = line_len/2;
	for (int y = 0; y < f.cb.height(); y++) {
		if (fread(in_line, chroma_line_len, 1, file) < 1)
			break;
		ptr = in_line;
		for (int x = 0; x < f.cb.width(); x++, ptr+=2)
			f.cb[y][x] = ((ptr[0] << 8) | ptr[1]);
	}

	/* read c2 plane */
	for (int y = 0; y < f.cr.height(); y++) {
		if (fread(in_line, chroma_line_len, 1, file) < 1)
			break;
		ptr = in_line;
		for (int x = 0; x < f.cr.width(); x++, ptr+=2)
			f.cr[y][x] = ((ptr[0] << 8) | ptr[1]);
	}

	free(in_line);
	return f;
}

RawFrame& RawReader16P0::read()
{
	throw;
}

void RawWriter16P0::write(const RawFrame& f) const
{
	assert(f.chroma == RawFrame::Cr420);
	const int line_len = f.luma.width() * 2;
	unsigned char* out_line = new unsigned char[line_len];
	unsigned char* ptr;

	/* write luma plane */
	for (int y = 0; y < f.luma.height(); y++) {
		ptr = out_line;
		for (int x = 0; x < f.luma.width(); x++, ptr+=2) {
			int v = f.luma[y][x];
			ptr[0] = v >> 8;
			ptr[1] = v & 0xff;
		}
		fwrite(out_line, line_len, 1, file);
	}

	/* write c1 plane */
	const int chroma_line_len = line_len/2;
	for (int y = 0; y < f.cb.height(); y++) {
		ptr = out_line;
		for (int x = 0; x < f.cb.width(); x++, ptr+=2) {
			int v = f.cb[y][x];
			ptr[0] = v >> 8;
			ptr[1] = v & 0xff;
		}
		fwrite(out_line, chroma_line_len, 1, file);
	}

	/* write c2 plane */
	for (int y = 0; y < f.cr.height(); y++) {
		ptr = out_line;
		for (int x = 0; x < f.cr.width(); x++, ptr+=2) {
			int v = f.cr[y][x];
			ptr[0] = v >> 8;
			ptr[1] = v & 0xff;
		}
		fwrite(out_line, chroma_line_len, 1, file);
	}

	free(out_line);
}
