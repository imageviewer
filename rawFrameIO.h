/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __RAWFRAMEIO_H
#define __RAWFRAMEIO_H

#include "rawFrame.h"

class RawReader {
public:
	virtual RawFrame& read() = 0;
	virtual RawFrame& read(RawFrame&) = 0;

	virtual ~RawReader()
	{
	};

	int getDepth()
	{
		return _depth;
	}
	
	int getWidth()
	{
		return _width;
	}
	
	int getHeight()
	{
		return _height;
	}

protected:
	int _depth;
	int _width;
	int _height;
};

class RawWriter {
public:
	virtual void write(const RawFrame&) const = 0;

	virtual ~RawWriter()
	{
	}
	;

protected:
	int _maxVal;
	int _width;
	int _height;
};

#endif
