/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __RAWIOSGI_H
#define __RAWIOSGI_H

#include <stdio.h>
#include "rawFrameIO.h"

class RawWritersgi : public RawWriter {
public:
	RawWritersgi(FILE *fd) :
		outfile(fd)
	{
	};

	void write(const RawFrame& f) const
	{
		(void)f;
	  //writing is not implemented yet
		throw;
	}

private:

	FILE *outfile;
};

class RawReadersgi : public RawReader {
public:
	RawReadersgi(FILE *fd) :
		infile(fd)
	{
		readHeader(); /* get the dimensions */
	};

	RawFrame& read(RawFrame&);
	RawFrame& read();

private:
	void readHeader();
	void read_plane(Array2d<int> &p, int ysize);

	int _magic;
	char _storage;
	char _bpc;
	unsigned int _dimension;
	unsigned int _zsize;
	long _pixmin;
	long _pixmax;
	char _name[81];
	long _colormap;

	FILE *infile;
};

#endif
