/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __CHROMARESAMPLE_H
#define __CHROMARESAMPLE_H

#include "rawFrame.h"

class ChromaResample {
public:

	ChromaResample(int inWidth, int inHeight, RawFrame::Chroma destType) :
		_width(inWidth), _height(inHeight), _resampled(inWidth, inHeight,
		                                               destType)
	{
	};

	const RawFrame& process(const RawFrame &src);
	void process_ext(const RawFrame &src, RawFrame& dest);

protected:

	void copyArray(const Array2d<int>& src, Array2d<int>& dest);

	//frame conversion functions
	void to444(const RawFrame& src, RawFrame& dest);
	void to422(const RawFrame& src, RawFrame& dest);
	void to420(const RawFrame& src, RawFrame& dest);
	void to411(const RawFrame& src, RawFrame& dest);
	void toYonly(const RawFrame& src, RawFrame& dest);

	//array interpolation / subsampling functions
	void horizSubsample(const Array2d<int>& in, Array2d<int>& out);
	void vertSubsample(const Array2d<int>& in, Array2d<int>& out);
	void horizInterp(const Array2d<int>& in, Array2d<int>& out);
	void vertInterp(const Array2d<int>& in, Array2d<int>& out);

	/* input size */
	const int _width;
	const int _height;

	/* output */
	RawFrame _resampled;
};

#endif
