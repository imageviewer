/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include "colourSpace.h"
#include "rawFrame.h"

#include <math.h>
#include <stdio.h>

#define DEBUG 0

#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define CLIP(x,min,max) MAX(MIN(x,max),min)

//build a colour matrix to convert from RGB to YUV
void ColourSpace::buildYUVMatrix(ColourMatrix &matrix, const float Kr,
                                 const float Kg, const float Kb)
{
	//Y row
	matrix.a = Kr; //R    column
	matrix.b = Kg; //G
	matrix.c = Kb; //B

	// cb row
	matrix.d = -1.0 * (Kr/(2.0*(1.0-Kb)));
	matrix.e = -1.0 * (Kg/(2.0*(1.0-Kb)));
	matrix.f = 0.5;

	//cr row
	matrix.g = 0.5;
	matrix.h = -1.0 * (Kg/(2.0*(1.0-Kr)));
	matrix.i = -1.0 * (Kb/(2.0*(1.0-Kr)));

	if (DEBUG) {
		printf("Building YUV->RGB matrix with Kr=%f Kg=%f, Kb=%f\n", Kr, Kg, Kb);
		printf("%f %f %f\n", matrix.a, matrix.b, matrix.c);
		printf("%f %f %f\n", matrix.d, matrix.e, matrix.f);
		printf("%f %f %f\n", matrix.g, matrix.h, matrix.i);
		printf("\n");
	}
}

//build a colour matrix to convert from YUV to RGB
void ColourSpace::buildRGBMatrix(ColourMatrix &matrix, const float Kr,
                                 const float Kg, const float Kb)
{
	//R row
	matrix.a = 1.0; //Y  column
	matrix.b = 0.0; //Cb
	matrix.c = 2.0 * (1-Kr); //Cr

	//G row
	matrix.d = 1.0;
	matrix.e = -1.0 * ( (2.0*(1.0-Kb)*Kb)/Kg );
	matrix.f = -1.0 * ( (2.0*(1.0-Kr)*Kr)/Kg );

	//B row
	matrix.g = 1.0;
	matrix.h = 2.0 * (1.0-Kb);
	matrix.i = 0.0;

	if (DEBUG) {
		printf("Building YUV->RGB matrix with Kr=%f Kg=%f, Kb=%f\n", Kr, Kg, Kb);
		printf("%f %f %f\n", matrix.a, matrix.b, matrix.c);
		printf("%f %f %f\n", matrix.d, matrix.e, matrix.f);
		printf("%f %f %f\n", matrix.g, matrix.h, matrix.i);
		printf("\n");
	}
}

const RawFrame& ColourSpace::process(const RawFrame &src,
                                     const MatrixType type,
                                     const bool scaledRGB, const bool YOnly,
                                     const int depth)
{
	process_ext(src, _converted, type, scaledRGB, YOnly, depth);
	return _converted;
}

//using a particular colour matrix, convert YUV->RGB or RGB->YUV
//the YUV data is assumed to have 'video' levels, with black at 16 for 8 bit pictures
//RGB data can be scaled (0-255) or unscaled (16-235) with respect to the YUV
void ColourSpace::process_ext(const RawFrame &src, RawFrame &dest,
                              const MatrixType type, const bool scaledRGB,
                              const bool YOnly, const int depth)
{
	//we only deal with YUV444 or RGB sources
	assert(src.chroma == RawFrame::Cr444 || src.chroma == RawFrame::CrRGB);

	const int maxVal = (1 << depth) - 1; //clip level for results
	const int Yoffset = scaledRGB ? 1 << (4 + (depth-8)) : 0; //Y black level
	const int Coffset = 1 << (7 + (depth-8)); //C mid-point


	const int limit_depth = (depth > 14) ? 14 : depth;
	const double intScaling = (float)(1 << (8 + (limit_depth-8))); //scale floating point colour matrix to integer
	const int rounding = 1 << (7 + (limit_depth-8)); //scaled 0.5

	//scaling factors for when converting from YUV to RGB
	double YUVYscale = 1.0; //applied to column 1 of the YUV->RGB matrix
	double YUVCscale = 1.0; //applied to column 2&3 of the YUV->RGB matrix

	//scaling factors for when converting from RGB to YUV
	double RGBYscale = 1.0; //applied to row 1 of the RGB->YUV matrix
	double RGBCscale = 1.0; //applied to row 2&3 of the RGB->YUV matrix

	if ((type == SDTVtoRGB || type == HDTVtoRGB) && scaledRGB == true) {
		//scale up to 0-255 RGB values from video levels
		YUVYscale = 255.0/219.0;
		YUVCscale = 255.0/224.0;
	}

	if ((type == SDTVtoYUV || type == HDTVtoYUV) && scaledRGB == true) {
		//scale 0-255 RGB values down to video levels
		RGBYscale = 219.0/255.0;
		RGBCscale = 224.0/255.0;
	}

	//select the required matrix
	ColourMatrix& matrix = matrices[type];

	//scale the matrix as necessary and convert to integers
	ColourMatrixInt intMatrix;
	intMatrix.a = (int)(floor(matrix.a * intScaling * YUVYscale * RGBYscale
	    + 0.5));
	intMatrix.b = (int)(floor(matrix.b * intScaling * YUVCscale * RGBYscale
	    + 0.5));
	intMatrix.c = (int)(floor(matrix.c * intScaling * YUVCscale * RGBYscale
	    + 0.5));
	intMatrix.d = (int)(floor(matrix.d * intScaling * YUVYscale * RGBCscale
	    + 0.5));
	intMatrix.e = (int)(floor(matrix.e * intScaling * YUVCscale * RGBCscale
	    + 0.5));
	intMatrix.f = (int)(floor(matrix.f * intScaling * YUVCscale * RGBCscale
	    + 0.5));
	intMatrix.g = (int)(floor(matrix.g * intScaling * YUVYscale * RGBCscale
	    + 0.5));
	intMatrix.h = (int)(floor(matrix.h * intScaling * YUVCscale * RGBCscale
	    + 0.5));
	intMatrix.i = (int)(floor(matrix.i * intScaling * YUVCscale * RGBCscale
	    + 0.5));

	if (DEBUG) {
		printf("Using integer matrix :\n");
		printf("%4d %4d %4d\n", intMatrix.a, intMatrix.b, intMatrix.c);
		printf("%4d %4d %4d\n", intMatrix.d, intMatrix.e, intMatrix.f);
		printf("%4d %4d %4d\n", intMatrix.g, intMatrix.h, intMatrix.i);
		printf("\n");
	}

	for (int line=0; line<src.luma.height(); line++) {
		for (int x=0; x<src.luma.width(); x++) {

			if (src.chroma == RawFrame::Cr444) {
				//source data is YUV
				int Y = src.luma[line][x] - Yoffset;
				int U = src.cb[line][x];
				int V = src.cr[line][x];

				if (YOnly == true) {
					U = 0;
					V = 0;
				}
				else {
					U -= Coffset;
					V -= Coffset;
				}

				//apply the matrix
				int R = ((intMatrix.a*Y + intMatrix.b*U + intMatrix.c*V
				    + rounding) >> limit_depth);
				int G = ((intMatrix.d*Y + intMatrix.e*U + intMatrix.f*V
				    + rounding) >> limit_depth);
				int B = ((intMatrix.g*Y + intMatrix.h*U + intMatrix.i*V
				    + rounding) >> limit_depth);

				//Clip and store
				dest.r[line][x] = CLIP(R, 0, maxVal);
				dest.g[line][x] = CLIP(G, 0, maxVal);
				dest.b[line][x] = CLIP(B, 0, maxVal);
			}
			else {
				//source data is RGB
				int R = src.r[line][x];
				int G = src.g[line][x];
				int B = src.b[line][x];

				//apply the matrix
				int Y = ((intMatrix.a*R + intMatrix.b*G + intMatrix.c*B
				    + rounding) >> limit_depth) + Yoffset;
				int U = Coffset;
				int V = Coffset;

				if (YOnly == false) {
					U += ((intMatrix.d*R + intMatrix.e*G + intMatrix.f*B
					    + rounding) >> limit_depth);
					V += ((intMatrix.g*R + intMatrix.h*G + intMatrix.i*B
					    + rounding) >> limit_depth);
				}

				//Clip and store
				dest.luma[line][x]= CLIP(Y, 0, maxVal);
				dest.cb[line][x] = CLIP(U, 0, maxVal);
				dest.cr[line][x] = CLIP(V, 0, maxVal);
			}
		}
	}
}
