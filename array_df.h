/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef ARRAY_DF_H_
#define ARRAY_DF_H_

#include <memory>
#include <stdlib.h>

/* 
 * Some housekeeping around a 1d array of elements, but no
 * accessors to this, you need to subclass Allocatable to
 * give it some structure.
 */
template<class T> class Allocatable {
public:

	Allocatable(int len) :
		_data(new T[len]), _len(len), _allocated(true)
	{
	}

	Allocatable(int len, T* data) :
		_data(data), _len(len), _allocated(false)
	{
	}

	~Allocatable()
	{
		if (_allocated)
			delete[] _data;
	}

	/* assign a single value to every element */
	Allocatable<T>& assign(const T& v)
	{
		for (int i = 0; i < _len; i++)
			_data[i] = v;

		return *this;
	}

	/* copy elements in one array to another */
	Allocatable<T>& operator=(const Allocatable& v)
	{
		if (_len != v._len)
			throw /* */;

		for (int i = 0; i < _len; i++)
			_data[i] = v._data[i];

		return *this;
	}
protected:
	T* const _data;

private:
	/* disable inadvertant copying */
	Allocatable<T>(const Allocatable<T>&);

	const int _len;
	const bool _allocated;
};

/*
 * Non resizable, One dimensional array.
 */
template<class T> class Array1d : public Allocatable<T> {
public:
	Array1d(int len) :
		Allocatable<T>(len), _len(len)
	{
	}
	;

	Array1d(int len, T* data) :
		Allocatable<T>(len, data), _len(len)
	{
	}
	;

	int length() const
	{
		return _len;
	}
	;

	T& operator[](const int i)
	{
		return *(_data + i);
	}
	;

	const T& operator[](const int i) const
	{
		return *(_data + i);
	}
	;

private:
	/* disable inadvertant copying */
	Array1d<T>(const Array1d<T>&);

	const int _len;

	using Allocatable<T>::_data;
};

/*
 * Non resizable, Two dimensional array.
 */
template<class T> class Array2d : public Allocatable<T> {
public:
	Array2d(int width, int height) :
		Allocatable<T>(width * height), _width(width), _height(height)
	{
		_rows = (Array1d<T>*) malloc(sizeof(Array1d<T>) * _height);
		for (int i = 0; i < _height; i++)
			/* magic! */
			::new(static_cast<void*>(&_rows[i]))
			Array1d<T>(_width, _data + i*width);
	}
	;

	/* clean up at destruction too -- this does not call destructors
	 * which it probably should. */
	~Array2d()
	{
		free(_rows);
	}
	;

	int width() const
	{
		return _width;
	}
	;
	int height() const
	{
		return _height;
	}
	;

	Array1d<T>& operator[](const int j)
	{
		return _rows[j];
	}
	;

	const Array1d<T>& operator[](const int j) const
	{
		return _rows[j];
	}
	;

	Array2d<T>& operator=(const Array2d<T>& v)
	{
		if (v.width() != _width || v.height() != _height)
			throw /* */;

		this->Allocatable<T>::operator=(v);

		return *this;
	}

private:
	/* disable inadvertant copying */
	Array2d<T>(const Array2d<T>&);

	const int _width;
	const int _height;

	/* an array of rowpointers, _height long.  This allows us to
	 * do more efficient row access. */
	Array1d<T> *_rows;

	/* gain access to _data from allocatable */
	using Allocatable<T>::_data;
};

#endif /*ARRAY_DF_H_*/
