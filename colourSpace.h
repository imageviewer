/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __COLOURSPACE_H
#define __COLOURSPACE_H

#include "rawFrame.h"

// USEFUL INFORMATION
// THERE ARE TWO COLOUR SYSTEMS WE ARE INTERESTED IN, 'SDTV' and 'HDTV', WHICH
// COVER ALL REAL APPLICATIONS

//SDTV :-
//ITU-R BT 601
//ITU-R BT 470, PAL system B/G
//SMPTE 170M, NTSC
//SMPTE 293M, 720x483 59.94P
//
//Ey  =  0.299Er + 0.587Eg + 0.114Eb

//HDTV :-
//ITU-R BT 709 (1125/60/2:1 only)
//ITU-R BT 1361 Worldwide Unified HDTV
//SMPTE 274M 1920x1080 HDTV
//SMPTE 296M 1280x720 HDTV
//
//Ey  =  0.2126Er + 0.7152Eg + 0.0722Eb

//For converting RGB to YCbCr
//
// Ey = Kg.Eg + Kb.Eb + Kr.Er
// and
// Kg + Kb + Kr = 1
//
// Ecb = (Eb-Ey) / (2(1-Kb))
// Ecr = (Er-Ey) / (2(1-Kr))

//For converting YCbCr to RGB
//
// Er = Ey + 2(1-Kr)Ecr
// Eg = Ey - (2(1-Kb)Kb.Ecb)/Kg - (2(1-Kr)Kr.Ecr)/Kg
// Eb = Ey + 2(1-Kb)Ecb

//This class builds the colour matrix using the equations above from Kr, Kg, Kb

class ColourSpace {
public:

	enum MatrixType {SDTVtoYUV, SDTVtoRGB, HDTVtoYUV, HDTVtoRGB};

	ColourSpace(int inWidth, int inHeight, RawFrame::Chroma destChroma) :
		_width(inWidth), _height(inHeight), _converted(inWidth, inHeight,
		                                               destChroma)
	{
		//construct matrices
		buildYUVMatrix(matrices[SDTVtoYUV], (float)0.299, (float)0.587,
		               (float)0.114);
		buildRGBMatrix(matrices[SDTVtoRGB], (float)0.299, (float)0.587,
		               (float)0.114);
		buildYUVMatrix(matrices[HDTVtoYUV], (float)0.2126, (float)0.7152,
		               (float)0.0722);
		buildRGBMatrix(matrices[HDTVtoRGB], (float)0.2126, (float)0.7152,
		               (float)0.0722);
	}
	;

	//reference matrix coefficients generated from Kr, Kg, Kb
	typedef struct _ColourMatrix {
		double a, b, c;
		double d, e, f;
		double g, h, i;
	} ColourMatrix;

	//integer matrix coefficients used for the actual processing
	//these may have been scaled to adjust number ranges during conversion
	typedef struct _ColourMatrixInt {
		int a, b, c;
		int d, e, f;
		int g, h, i;
	} ColourMatrixInt;

	const RawFrame& process(const RawFrame &src, const MatrixType type,
	                        const bool scaledRGB, const bool YOnly,
	                        const int depth);
	void process_ext(const RawFrame &src, RawFrame& dest,
	                 const MatrixType type, const bool scaledRGB,
	                 const bool YOnly, const int depth);

protected:
	void buildRGBMatrix(ColourMatrix &matrix, const float Kr, const float Kg,
	                    const float Kb);
	void buildYUVMatrix(ColourMatrix &matrix, const float Kr, const float Kg,
	                    const float Kb);

	/* input size */
	const int _width;
	const int _height;

	/* colour matrices */
	ColourMatrix matrices[4];

	/* output */
	RawFrame _converted;
};

#endif
