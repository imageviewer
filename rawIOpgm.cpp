/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <assert.h>
#include <math.h>

#include "rawIOpgm.h"

void RawWriterpgm::writeCr42x(const RawFrame& f) const
{
	/* YYYY (Planar)
	 * CbCr
	 */
	fprintf(outfile, "P5 %d %d %d\n", f.luma.width(), f.luma.height()
	    + f.cr.height(), (1 << depth) - 1);

	if (depth > 8) {
		int mask = (1 << (depth - 8)) - 1;
		int zero_shift = 0;
		/* luma first */
		for (int y = 0; y < f.luma.height(); y++)
			for (int x = 0; x < f.luma.width(); x++) {
				fputc((f.luma[y][x] >> 8) & mask, outfile);
				fputc((f.luma[y][x] & 0xff), outfile);
			}

		/* chroma second (side by side) */
		for (int y = 0; y < f.cb.height(); y++) {
			for (int x = 0; x < f.cb.width(); x++) {
				int val = zero_shift + f.cb[y][x];
				fputc((val >> 8) & mask, outfile);
				fputc(val & 0xff, outfile);
			}

			for (int x = 0; x < f.cr.width(); x++) {
				int val = zero_shift + f.cr[y][x];
				fputc((val >> 8) & mask, outfile);
				fputc(val & 0xff, outfile);
			}
		}
	}
	else {
		int zero_shift = 0;
		/* luma first */
		for (int y = 0; y < f.luma.height(); y++)
			for (int x = 0; x < f.luma.width(); x++) {
				fputc((f.luma[y][x] & 0xff), outfile);
			}

		/* chroma second (side by side) */
		for (int y = 0; y < f.cb.height(); y++) {
			for (int x = 0; x < f.cb.width(); x++) {
				fputc((zero_shift + f.cb[y][x]) & 0xff, outfile);
			}

			for (int x = 0; x < f.cr.width(); x++) {
				fputc((zero_shift + f.cr[y][x]) & 0xff, outfile);
			}
		}

	}
}

void RawWriterpgm::writeYOnly(const RawFrame& f) const
{
	/* YYYY (Planar)
	 * CbCr
	 */
	assert(f.chroma == RawFrame::YOnly);

	fprintf(outfile, "P5 %d %d %d\n", f.luma.width(), f.luma.height(), (1
	    << depth) - 1);

	if (depth > 8) {
		int mask = (1 << (depth - 8)) - 1;
		for (int y = 0; y < f.luma.height(); y++)
			for (int x = 0; x < f.luma.width(); x++) {
				fputc((f.luma[y][x] >> 8) & mask, outfile);
				fputc((f.luma[y][x] & 0xff), outfile);
			}
	}
	else {
		for (int y = 0; y < f.luma.height(); y++)
			for (int x = 0; x < f.luma.width(); x++) {
				fputc((f.luma[y][x] & 0xff), outfile);
			}
	}
}

void RawReaderpgm::readHeader()
{
	// This works for our files, but is not generalised:
	fscanf(infile, "P5 %d %d %d\n", &_width, &_height, &_maxval);

	_depth = (int)ceil(log((double)_maxval) / log(2.0));
}

RawFrame& RawReaderpgm::read(RawFrame &f)
{
	//luminance data only
	assert(f.chroma == RawFrame::YOnly);

	size_t linesRead;
	size_t lineLen;

	if (_maxval > 255) {
		lineLen = f.luma.width() * 2;
		unsigned char *inLine = new unsigned char[lineLen];

		for (int y = 0; y < _height; y++) {

			linesRead = fread(inLine, lineLen, 1, infile);

			for (int in=0, x=0; x < _width; x++, in+=2)
				f.luma[y][x]=inLine[in]*256 + inLine[in+1];

			if (linesRead < 1)
				goto exit1;
		}
		exit1: free(inLine);
	}
	else {
		lineLen = f.luma.width();
		unsigned char *inLine = new unsigned char[lineLen];

		for (int y = 0; y < _height; y++) {

			linesRead = fread(inLine, lineLen, 1, infile);

			for (int x = 0; x < _width; x++)
				f.luma[y][x]=inLine[x];

			if (linesRead < 1)
				goto exit2;
		}
		exit2: free(inLine);
	}

	//if (feof(infile))
	//    throw;
	return f;
}

RawFrame& RawReaderpgm::read()
{
	throw;
}

