/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#ifndef __RAWIOPPM_H
#define __RAWIOPPM_H

#include <stdio.h>
#include "rawFrameIO.h"

class RawWriterppm : public RawWriter {
public:
	RawWriterppm(FILE *fd, int d) :
		outfile(fd), depth(d)
	{
		/* pgm may only have a maxval of 65535 */
		assert(d <= 16);
	};

	void write(const RawFrame& f) const
	{
		switch (f.chroma) {
		case RawFrame::Cr422:
			writeCr422(f);
			break; /* 422 is done */
		case RawFrame::Cr420:
			throw;
			break; /* 420 is not yet implemented */
		case RawFrame::YOnly:
			writeYOnly(f);
			break; /* Y-only is done */
		default:
			throw /* invalid frame type */;
		}
	}

private:
	void writeCr422(const RawFrame&) const;
	void writeYOnly(const RawFrame&) const;
	void
	    chroma422to444(const Array2d<int>& in, const int line, Array1d<int>& out) const;

	void yuv2rgb(const Array2d<int>& Y, const int lineNum,
	             const Array1d<int>& ULine, const Array1d<int>& VLine,
	             Array1d<int>& RLine, Array1d<int>& GLine, Array1d<int>& BLine,
	             int depth) const;

	FILE *outfile;
	int depth;
};

class RawReaderppm : public RawReader {
public:
	RawReaderppm(FILE *fd) :
		infile(fd)
	{
		readHeader(); /* get the dimensions */
	}
	;

	RawFrame& read(RawFrame&);
	RawFrame& read();

private:
	void readHeader();

	int _maxval;
	FILE *infile;
};

#endif
