/* ***** BEGIN LICENSE BLOCK *****
 *
 * $Id$
 *
 * The MIT License
 *
 * Copyright (c) 2008 BBC Research
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ***** END LICENSE BLOCK ***** */

#include <QtGui>

#include "customSizeDialog.h"

CustomSizeDialog::CustomSizeDialog(const QString &name, int width, int height,
                                   QWidget *parent) : QDialog(parent)
{
	setWindowTitle(name);

	//enforce multiple of 4 width/height
	if (width % 4 != 0)
		width = (width / 4) * 4;

	if (height % 4 != 0)
		height = (height / 4) * 4;

	//Width
	widthLabel = new QLabel(tr("Width"));

	widthNumber = new QSpinBox;
	widthNumber->setMinimum(4);
	widthNumber->setMaximum(8192);
	widthNumber->setSingleStep(4);
	widthNumber->setValue(width);

	//Height
	heightLabel = new QLabel(tr("Height"));

	heightNumber = new QSpinBox;
	heightNumber->setMinimum(4);
	heightNumber->setMaximum(8192);
	heightNumber->setSingleStep(4);
	heightNumber->setValue(height);

	//buttons
	okButton = new QPushButton("OK", this);
	connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));

	cancelButton = new QPushButton("Cancel", this);
	connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

	//layout
	QHBoxLayout *widthLayout = new QHBoxLayout;
	widthLayout->addWidget(widthLabel);
	widthLayout->addWidget(widthNumber);

	QHBoxLayout *heightLayout = new QHBoxLayout;
	heightLayout->addWidget(heightLabel);
	heightLayout->addWidget(heightNumber);

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->addStretch();
	buttonLayout->addWidget(okButton);
	buttonLayout->addWidget(cancelButton);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->addLayout(widthLayout);
	layout->addLayout(heightLayout);
	layout->addLayout(buttonLayout);

	setLayout(layout);
}

int CustomSizeDialog::getWidth(void)
{
	return (widthNumber->value());
}

int CustomSizeDialog::getHeight(void)
{
	return (heightNumber->value());
}
